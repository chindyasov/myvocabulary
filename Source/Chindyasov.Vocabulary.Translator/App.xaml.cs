﻿using System;
using System.Windows;
using System.Reflection;
using System.Windows.Interop;
using Autofac;
using Chindyasov.Utils;
using Chindyasov.Utils.Net;
using Chindyasov.Vocabulary.Translator.Views;
using Chindyasov.Vocabulary.Translator.Models;
using Chindyasov.Vocabulary.Translator.Services;
using Chindyasov.Vocabulary.Translator.ViewModels;
using Chindyasov.Vocabulary.TranslationProviders;
using Chindyasov.Vocabulary.TranslationProviders.Yandex;

namespace Chindyasov.Vocabulary.Translator
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private HotKeyHandler _hotKeyHandler;
        private IntPtr _mainWindowHandle = IntPtr.Zero;

        private IContainer Container { get; set; }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            var builder = new ContainerBuilder();

            RegisterViews(builder);
            RegisterViewModels(builder);
            RegisterComponents(builder);

            Container = builder.Build();

            LinkViewModels();

            InitMainWindow();
        }

        private void InitMainWindow()
        {
            var mainWindowViewModel = Container.Resolve<MainWindowViewModel>();
            mainWindowViewModel.Title = "MyVocabulary Translator";
            mainWindowViewModel.ShowWithStartupState();
        }

        private void RegisterViews(ContainerBuilder builder)
        {
            var mainWindow = new MainWindowView();
            mainWindow.Loaded += HandleMainWindowLoaded;

            builder.RegisterInstance(mainWindow);
            builder.RegisterType<TranslationView>().SingleInstance();
            builder.RegisterType<ProgressWindowView>().SingleInstance();
            builder.RegisterType<MessageView>();
        }

        private void RegisterViewModels(ContainerBuilder builder)
        {
            builder.RegisterType<MainWindowViewModel>().SingleInstance();
            builder.RegisterType<CloseButtonViewModel>().SingleInstance();
            builder.RegisterType<PhraseInputViewModel>().SingleInstance();
            builder.RegisterType<TranslationViewModel>().SingleInstance();
            builder.RegisterType<ProgressWindowViewModel>().SingleInstance();
            builder.RegisterType<MessageViewModel>().SingleInstance();
            builder.RegisterType<UpdateWindowViewModel>().SingleInstance();
        }

        private void RegisterComponents(ContainerBuilder builder)
        {
            _hotKeyHandler = new HotKeyHandler();

            builder.RegisterType<UrlOpener>().As<IUrlOpener>().SingleInstance();
            builder.RegisterType<Dimensions>().As<IDimensions>().SingleInstance();
            builder.RegisterType<BrowserOpener>().As<IBrowserOpener>().SingleInstance();
            builder.RegisterType<UpdateManager>().As<IUpdateManager>().SingleInstance();
            builder.RegisterType<UpdateCheckingTimer>().As<IUpdateCheckingTimer>().SingleInstance();
            builder.Register(c => new AppVersion(Assembly.GetEntryAssembly().GetName().Version.ToString())).SingleInstance();
            builder.RegisterInstance(_hotKeyHandler).As<IHotKeyHandler>();

            RegisterTranslationComponents(builder);
        }

        private void RegisterTranslationComponents(ContainerBuilder builder)
        {
            builder.RegisterType<YandexTranslateProvider>().SingleInstance();
            builder.RegisterType<YandexDictionaryProvider>().SingleInstance();
            builder.Register(c => new AggregateTranslationProvider(c.Resolve<YandexDictionaryProvider>(), c.Resolve<YandexTranslateProvider>())).As<ITranslationProvider>();
            builder.RegisterType<TranslationLoader>().As<ITranslationLoader>().SingleInstance();
        }

        private void LinkViewModels()
        {
            var mainWindowView = Container.Resolve<MainWindowView>();

            mainWindowView.DataContext = Container.Resolve<MainWindowViewModel>();
            Container.Resolve<TranslationView>().DataContext = Container.Resolve<TranslationViewModel>();
            Container.Resolve<ProgressWindowView>().DataContext = Container.Resolve<ProgressWindowViewModel>();
            Container.Resolve<MessageView>().DataContext = Container.Resolve<MessageViewModel>();
            Container.Resolve<MessageView>().DataContext = Container.Resolve<UpdateWindowViewModel>();
            mainWindowView.closeButton.DataContext = Container.Resolve<CloseButtonViewModel>();
            mainWindowView.phraseInputView.DataContext = Container.Resolve<PhraseInputViewModel>();
        }

        private void HandleMainWindowLoaded(object sender, RoutedEventArgs e)
        {
            if (_mainWindowHandle == IntPtr.Zero)
            {
                _mainWindowHandle = new WindowInteropHelper(sender as Window).Handle;
                _hotKeyHandler.RegisterForHandle(_mainWindowHandle);
            }
        }
    }
}
