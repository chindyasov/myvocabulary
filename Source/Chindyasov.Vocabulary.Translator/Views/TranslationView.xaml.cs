﻿using System;
using System.Windows;
using System.Windows.Navigation;

namespace Chindyasov.Vocabulary.Translator.Views
{
    public class TranslationViewBase : WindowViewBase
    {
        public static DependencyProperty VendorLinkClickedActionProperty = DependencyProperty.Register("VendorLinkClickedAction", typeof(Action), typeof(TranslationViewBase));

        public Action VendorLinkClickedAction
        {
            get
            {
                return GetValue(VendorLinkClickedActionProperty) as Action;
            }
            set
            {
                SetValue(VendorLinkClickedActionProperty, value);
            }
        }
    }

    /// <summary>
    /// Interaction logic for TranslationView.xaml
    /// </summary>
    public partial class TranslationView : TranslationViewBase
    {
        public TranslationView()
        {
            InitializeComponent();
            IsVisibleChanged += Handle_IsVisibleChanged;
        }

        private void Handle_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((e.NewValue as bool?) == true) _scrollViewer.ScrollToTop();
        }

        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            VendorLinkClickedAction?.Invoke();
        }
    }
}
