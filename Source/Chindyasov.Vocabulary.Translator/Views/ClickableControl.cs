﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;

namespace Chindyasov.Vocabulary.Translator.Views
{
    public class ClickableControl : UserControl
    {
        public static readonly DependencyProperty MouseClickActionProperty = DependencyProperty.Register("MouseClickAction", typeof(Action), typeof(ClickableControl));

        private bool _mouseInControl = false;
        private bool _mouseButtonDownDetected = false;

        public ClickableControl()
        {
            MouseLeave += CloseButtonView_MouseLeave;
            MouseEnter += CloseButtonViewBase_MouseEnter;
            MouseLeftButtonDown += CloseButtonView_MouseLeftButtonDown;
            MouseLeftButtonUp += CloseButtonView_MouseLeftButtonUp;
        }

        public Action MouseClickAction
        {
            get
            {
                return GetValue(MouseClickActionProperty) as Action;
            }
            set
            {
                SetValue(MouseClickActionProperty, value);
            }
        }

        private void CloseButtonView_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (_mouseButtonDownDetected && _mouseInControl)
                MouseClickAction?.Invoke();

            _mouseButtonDownDetected = false;
        }

        private void CloseButtonView_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _mouseButtonDownDetected = true;
        }

        private void CloseButtonViewBase_MouseEnter(object sender, MouseEventArgs e)
        {
            _mouseInControl = true;
        }

        private void CloseButtonView_MouseLeave(object sender, MouseEventArgs e)
        {
            _mouseInControl = false;
        }
    }
}
