﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;

namespace Chindyasov.Vocabulary.Translator.Views
{
    public class CloseButtonViewBase : UserControl
    {
        public static readonly DependencyProperty MouseLeaveActionProperty = DependencyProperty.Register("MouseLeaveAction", typeof(Action), typeof(CloseButtonViewBase));
        public static readonly DependencyProperty MouseLeftButtonDownActionProperty = DependencyProperty.Register("MouseLeftButtonDownAction", typeof(Action), typeof(CloseButtonViewBase));
        public static readonly DependencyProperty MouseLeftButtonUpActionProperty = DependencyProperty.Register("MouseLeftButtonUpAction", typeof(Action), typeof(CloseButtonViewBase));
        public static readonly DependencyProperty MouseEnterActionProperty = DependencyProperty.Register("MouseEnterAction", typeof(Action), typeof(CloseButtonViewBase));

        public CloseButtonViewBase()
        {
            MouseLeave += CloseButtonView_MouseLeave;
            MouseEnter += CloseButtonViewBase_MouseEnter;
            MouseLeftButtonDown += CloseButtonView_MouseLeftButtonDown;
            MouseLeftButtonUp += CloseButtonView_MouseLeftButtonUp;
        }

        public Action MouseLeaveAction
        {
            get
            {
                return GetValue(MouseLeaveActionProperty) as Action;
            }
            set
            {
                SetValue(MouseLeaveActionProperty, value);
            }
        }

        public Action MouseEnterAction
        {
            get
            {
                return GetValue(MouseEnterActionProperty) as Action;
            }
            set
            {
                SetValue(MouseEnterActionProperty, value);
            }
        }

        public Action MouseLeftButtonDownAction
        {
            get
            {
                return GetValue(MouseLeftButtonDownActionProperty) as Action;
            }
            set
            {
                SetValue(MouseLeftButtonDownActionProperty, value);
            }
        }

        public Action MouseLeftButtonUpAction
        {
            get
            {
                return GetValue(MouseLeftButtonUpActionProperty) as Action;
            }
            set
            {
                SetValue(MouseLeftButtonUpActionProperty, value);
            }
        }

        private void CloseButtonView_MouseLeave(object sender, MouseEventArgs e)
        {
            MouseLeaveAction?.Invoke();
        }

        private void CloseButtonViewBase_MouseEnter(object sender, MouseEventArgs e)
        {
            MouseEnterAction?.Invoke();
        }

        private void CloseButtonView_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            MouseLeftButtonUpAction?.Invoke();
        }

        private void CloseButtonView_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MouseLeftButtonDownAction?.Invoke();
        }
    }

    public partial class CloseButtonView : CloseButtonViewBase
    {
        public CloseButtonView()
        {
            InitializeComponent();
        }
    }

}
