﻿using System;
using System.Windows;
using System.Windows.Documents;

namespace Chindyasov.Vocabulary.Translator.Views
{
    public class CustomLink : Hyperlink
    {
        public static DependencyProperty LinkClickedActionProperty = DependencyProperty.Register("LinkClickedAction", typeof(Action), typeof(CustomLink));

        public CustomLink()
        {
            Click += CustomLink_Click;
        }

        public CustomLink(Inline childInline) : base(childInline)
        {
            Click += CustomLink_Click;
        }

        public CustomLink(TextPointer start, TextPointer end) : base(start, end)
        {
            Click += CustomLink_Click;
        }

        public CustomLink(Inline childInline, TextPointer insertionPosition) : base(childInline, insertionPosition)
        {
            Click += CustomLink_Click;
        }

        public Action LinkClickedAction
        {
            get
            {
                return GetValue(LinkClickedActionProperty) as Action;
            }
            set
            {
                SetValue(LinkClickedActionProperty, value);
            }
        }

        private void CustomLink_Click(object sender, RoutedEventArgs e)
        {
            LinkClickedAction?.Invoke();
        }
    }
}
