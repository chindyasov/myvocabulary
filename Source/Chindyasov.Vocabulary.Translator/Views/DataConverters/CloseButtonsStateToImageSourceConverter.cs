﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Globalization;
using Chindyasov.Vocabulary.Translator.ViewModels;

namespace Chindyasov.Vocabulary.Translator.Views.DataConverters
{
    [ValueConversion(typeof(CloseButtonState), typeof(ImageSource))]
    public sealed class CloseButtonsStateToImageSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (Application.Current != null && value is CloseButtonState)
            {
                return TryExtractImageSource((CloseButtonState)value);
            }
            else
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private ImageSource TryExtractImageSource(CloseButtonState state)
        {
            string resourceName = null;

            if (state == CloseButtonState.Default)
                resourceName = "CloseButtonDefault";
            else if (state == CloseButtonState.Active)
                resourceName = "CloseButtonActive";
            else if (state == CloseButtonState.Clicked)
                resourceName = "CloseButtonClick";
            else
                return null;

            return Application.Current.TryFindResource(resourceName) as ImageSource;
        }
    }
}
