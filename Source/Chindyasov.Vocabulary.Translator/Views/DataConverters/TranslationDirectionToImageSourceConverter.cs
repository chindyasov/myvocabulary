﻿using System;
using System.Windows.Data;
using System.Windows.Media;

namespace Chindyasov.Vocabulary.Translator.Views.DataConverters
{
    [ValueConversion(typeof(TranslationDirection), typeof(ImageSource))]
    public sealed class TranslationDirectionToImageSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if ((TranslationDirection)value == TranslationDirection.EN_RU)
                return (ImageSource)App.Current.Resources["TranslationEnRu"];
            else
                return (ImageSource)App.Current.Resources["TranslationRuEn"];
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
