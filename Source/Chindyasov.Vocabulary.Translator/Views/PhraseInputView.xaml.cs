﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;

namespace Chindyasov.Vocabulary.Translator.Views
{
    public class PhraseInputViewBase : UserControl
    {
        public static readonly DependencyProperty MouseButtonDownActionProperty = DependencyProperty.Register("MouseButtonDownAction", typeof(Action), typeof(PhraseInputViewBase));
        public static readonly DependencyProperty LostFocusActionProperty = DependencyProperty.Register("LostFocusAction", typeof(Action), typeof(PhraseInputViewBase));
        public static readonly DependencyProperty EnterButtonPressedActionProperty = DependencyProperty.Register("EnterButtonPressedAction", typeof(Action), typeof(PhraseInputViewBase));

        public PhraseInputViewBase()
        {
            AddHandler(Mouse.MouseDownEvent, new RoutedEventHandler(PhraseInputViewBase_MouseDown), true);
            LostKeyboardFocus += PhraseInputViewBase_LostFocus;
            KeyDown += PhraseInputViewBase_KeyDown;
        }

        public Action MouseButtonDownAction
        {
            get
            {
                return GetValue(MouseButtonDownActionProperty) as Action;
            }
            set
            {
                SetValue(MouseButtonDownActionProperty, value);
            }
        }

        public Action LostFocusAction
        {
            get
            {
                return GetValue(LostFocusActionProperty) as Action;
            }
            set
            {
                SetValue(LostFocusActionProperty, value);
            }
        }

        public Action EnterButtonPressedAction
        {
            get
            {
                return GetValue(EnterButtonPressedActionProperty) as Action;
            }
            set
            {
                SetValue(EnterButtonPressedActionProperty, value);
            }
        }

        protected void PhraseInputViewBase_MouseDown(object sender, EventArgs e)
        {
            MouseButtonDownAction?.Invoke();
        }

        protected void PhraseInputViewBase_LostFocus(object sender, EventArgs e)
        {
            LostFocusAction?.Invoke();
        }

        private void PhraseInputViewBase_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                EnterButtonPressedAction?.Invoke();
            }
        }
    }

    /// <summary>
    /// Interaction logic for PhraseInputView.xaml
    /// </summary>
    public partial class PhraseInputView : PhraseInputViewBase
    {
        public PhraseInputView()
        {
            InitializeComponent();
            Loaded += (s, a) => phraseTextBox.Focus();
        }
    }
}
