﻿using System;
using System.Windows;
using System.Windows.Input;

namespace Chindyasov.Vocabulary.Translator.Views
{
    public class WindowViewBase : Window
    {
        public static readonly DependencyProperty EscapeKeyPressedActionProperty = DependencyProperty.Register("EscapeKeyPressedAction", typeof(Action), typeof(WindowViewBase));

        public WindowViewBase()
        {
            EscapeKeyPressedAction = new Action(() => {});
            KeyDown += WindowViewBase_KeyDown;
        }

        public Action EscapeKeyPressedAction
        {
            get
            {
                return GetValue(EscapeKeyPressedActionProperty) as Action;
            }
            set
            {
                SetValue(EscapeKeyPressedActionProperty, value);
            }
        }

        private void WindowViewBase_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                EscapeKeyPressedAction?.Invoke();
        }
    }
}
