﻿using System;
using System.Windows;

namespace Chindyasov.Vocabulary.Translator.Views
{
    public class MainWindowViewBase : WindowViewBase
    {
        public static DependencyProperty ViewLoadedActionProperty = DependencyProperty.Register("ViewLoadedAction", typeof(Action), typeof(MainWindowViewBase));

        public MainWindowViewBase()
        {
            Loaded += MainWindowViewBase_Loaded;
        }

        public Action ViewLoadedAction
        {
            get
            {
                return GetValue(ViewLoadedActionProperty) as Action;
            }
            set
            {
                SetValue(ViewLoadedActionProperty, value);
            }
        }

        private void MainWindowViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            ViewLoadedAction?.Invoke();
        }
    }

    /// <summary>
    /// Interaction logic for MainWindowView.xaml
    /// </summary>
    public partial class MainWindowView : MainWindowViewBase
    {
        public MainWindowView()
        {
            InitializeComponent();
        }
    }
}
