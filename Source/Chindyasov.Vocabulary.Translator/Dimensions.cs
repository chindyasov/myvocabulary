﻿using Chindyasov.Vocabulary.Translator.Views;
using System.Windows;

namespace Chindyasov.Vocabulary.Translator
{
    public class Dimensions : IDimensions
    {
        private readonly double _mainViewHeight;
        private readonly double _mainViewWidth;

        public Dimensions(MainWindowView view)
        {
            _mainViewWidth = view.Width;
            _mainViewHeight = view.Height;
        }

        public double ScreenHeight => SystemParameters.PrimaryScreenHeight;
        public double ScreenWidth => SystemParameters.PrimaryScreenWidth;
        public double MainViewHeight => _mainViewHeight;
        public double MainViewWidth => _mainViewWidth;
    }
}
