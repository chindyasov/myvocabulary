﻿namespace Chindyasov.Vocabulary.Translator
{
    public interface IDimensions
    {
        double ScreenWidth { get; }
        double ScreenHeight { get; }
        double MainViewWidth { get; }
        double MainViewHeight { get; }
    }
}
