﻿using System;
using System.Timers;

namespace Chindyasov.Vocabulary.Translator.Models
{
    public class UpdateCheckingTimer : IUpdateCheckingTimer, IDisposable
    {
        public event EventHandler NeedToCheckUpdate;
        public event EventHandler NeedToCheckUpdateOnError;

        private readonly Timer _updateTimer;
        private readonly Timer _errorUpdateTimer;

        public UpdateCheckingTimer()
        {
            _updateTimer = InitTimer(TimeSpan.FromDays(3).TotalMilliseconds, HandleTimerElapsed);
            _errorUpdateTimer = InitTimer(TimeSpan.FromMinutes(1).TotalMilliseconds, HandlerErrorTimerElapsed);
        }

        public void Dispose()
        {
            _updateTimer.Dispose();
            _errorUpdateTimer.Dispose();
        }

        private Timer InitTimer(double milliseconds, ElapsedEventHandler handler)
        {
            var timer = new Timer(milliseconds);

            timer.AutoReset = true;
            timer.Elapsed += handler;

            timer.Start();

            return timer;
        }

        private void HandleTimerElapsed(object sender, ElapsedEventArgs e)
        {
            OnNeedToCheckUpdate(NeedToCheckUpdate);
        }

        private void HandlerErrorTimerElapsed(object sender, ElapsedEventArgs e)
        {
            OnNeedToCheckUpdate(NeedToCheckUpdateOnError);
        }

        private void OnNeedToCheckUpdate(EventHandler handler)
        {
            handler?.Invoke(this, EventArgs.Empty);
        }
    }
}
