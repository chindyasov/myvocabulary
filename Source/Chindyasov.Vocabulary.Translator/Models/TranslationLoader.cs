﻿using System;
using System.Threading.Tasks;
using Chindyasov.Vocabulary.TranslationProviders;

namespace Chindyasov.Vocabulary.Translator.Models
{
    public class TranslationLoader : ITranslationLoader
    {
        private ITranslationProvider _translationProvider;
        
        public event EventHandler<ITranslationResult> TranslationLoaded;
        public event EventHandler<TranslationException> TranslationError;

        public TranslationLoader(ITranslationProvider translationProvider)
        {
            _translationProvider = translationProvider;
        }

        public void StartForPhrase(string phrase, TranslationDirection direction)
        {
            Task.Factory.StartNew(() => { LoadTranslation(phrase, direction); });
        }

        private void LoadTranslation(string phrase, TranslationDirection direction)
        {
            try
            {
                OnTranslationLoaded(TranslatePhrase(phrase, direction));
            }
            catch (TranslationException exception)
            {
                OnTranslationError(exception);
            }
        }

        private ITranslationResult TranslatePhrase(string phrase, TranslationDirection direction)
        {
            return _translationProvider.Translate(phrase, direction);
        }

        private void OnTranslationLoaded(ITranslationResult translationResult)
        {
            TranslationLoaded?.Invoke(this, translationResult);
        }

        private void OnTranslationError(TranslationException exception)
        {
            TranslationError?.Invoke(this, exception);
        }
    }
}