﻿namespace Chindyasov.Vocabulary.Translator.Models
{
    public interface IUpdateManager
    {
        void OpenUpdatePage();

        bool IsNeedToShowUpdateDialog { get; }
    }
}