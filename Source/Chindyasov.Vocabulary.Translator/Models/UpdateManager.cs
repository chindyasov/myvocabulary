﻿using System;
using Newtonsoft.Json;
using Chindyasov.Utils;
using Chindyasov.Utils.Net;
using Chindyasov.Vocabulary.Translator.Services;

namespace Chindyasov.Vocabulary.Translator.Models
{
    public class UpdateManager : IUpdateManager
    {
        private const string _configUrl = "http://apps.chindyasov.ru/myvocabulary/translator_update.json";

        private static object _lock = new object();

        private bool _isUpdateAvailable;
        private IUrlOpener _urlOpener;
        private IBrowserOpener _browserOpener;
        private AppVersion _currentVersion;
        private IUpdateCheckingTimer _updateCheckingTimer;

        private string _updatePageUrl;

        private bool _updateCheckErrorWasDetected = false;

        public UpdateManager(IUpdateCheckingTimer updateCheckingTimer, AppVersion currentVersion, IUrlOpener urlOpener, IBrowserOpener browserOpener)
        {
            _urlOpener = urlOpener;
            _browserOpener = browserOpener;
            _currentVersion = currentVersion;
            _updateCheckingTimer = updateCheckingTimer;

            _updateCheckingTimer.NeedToCheckUpdate += HandleNeedToCheckUpdate;
            _updateCheckingTimer.NeedToCheckUpdateOnError += HandleNeedToCheckUpdateOnError;

            UpdateSettings();
        }

        public bool IsNeedToShowUpdateDialog => _isUpdateAvailable;

        public void OpenUpdatePage()
        {
            if (IsNeedToShowUpdateDialog == false)
                throw new InvalidOperationException("update is not available");

            _browserOpener.Open(_updatePageUrl);
        }

        private void UpdateSettings()
        {
            try
            {
                var updateJson = _urlOpener.ReadToEnd(_configUrl);
                var settings = JsonConvert.DeserializeObject<UpdateSettings>(updateJson);

                lock (_lock)
                {
                    _updatePageUrl = settings.UpdatePageUrl;
                    _isUpdateAvailable = settings.Enabled && _currentVersion < new AppVersion(settings.StableVersion);
                    _updateCheckErrorWasDetected = false;
                }
            }
            catch
            {
                lock (_lock)
                {
                    _isUpdateAvailable = false;
                    _updateCheckErrorWasDetected = true;
                }
            }
        }

        private void HandleNeedToCheckUpdate(object sender, EventArgs e)
        {
            UpdateSettings();
        }

        private void HandleNeedToCheckUpdateOnError(object sender, EventArgs e)
        {
            if (_updateCheckErrorWasDetected)
                UpdateSettings();
        }
    }

    class UpdateSettings
    {
        [JsonProperty("Enabled", Required = Required.Always)]
        public bool Enabled { get; set; }

        [JsonProperty("StableVersion", Required = Required.Always)]
        public string StableVersion { get; set; }

        [JsonProperty("UpdatePageUrl", Required = Required.Always)]
        public string UpdatePageUrl { get; set; }
    }
}