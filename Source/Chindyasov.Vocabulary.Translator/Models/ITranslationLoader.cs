﻿using System;
using Chindyasov.Vocabulary.TranslationProviders;

namespace Chindyasov.Vocabulary.Translator.Models
{
    public interface ITranslationLoader
    {
        event EventHandler<ITranslationResult> TranslationLoaded;
        event EventHandler<TranslationException> TranslationError;

        void StartForPhrase(string phrase, TranslationDirection direction);
    }
}