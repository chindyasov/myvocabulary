﻿using System;

namespace Chindyasov.Vocabulary.Translator.Models
{
    public interface IUpdateCheckingTimer
    {
        event EventHandler NeedToCheckUpdate;
        event EventHandler NeedToCheckUpdateOnError;
    }
}
