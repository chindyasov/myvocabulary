﻿using System;
using Chindyasov.Vocabulary.Translator.Models;

namespace Chindyasov.Vocabulary.Translator.ViewModels
{
    public class UpdateWindowViewModel : WindowViewModelBase
    {
        private IUpdateManager _updateManager;

        public event EventHandler EscButtonPressed;
        public event EventHandler DownloadLinkClicked;

        public UpdateWindowViewModel(IDimensions dimensions, IUpdateManager updateManager) : base(dimensions)
        {
            _updateManager = updateManager;

            Top = dimensions.MainViewHeight;
            IsVisible = false;
        }

        public Action DownloadAction => HandleDownloadAction;

        public void ShowViewIfNeeded()
        {
            if (_updateManager.IsNeedToShowUpdateDialog)
                IsVisible = true;
        }

        public void HideView()
        {
            IsVisible = false;
        }

        protected override void HandleEscapeKeyPressedAction()
        {
            EscButtonPressed?.Invoke(this, EventArgs.Empty);
        }

        private void HandleDownloadAction()
        {
            _updateManager.OpenUpdatePage();

            OnDownloadLinkClicked();
        }

        private void OnDownloadLinkClicked()
        {
            DownloadLinkClicked?.Invoke(this, EventArgs.Empty);
        }
    }
}
