﻿using System;
using System.Threading;
using System.ComponentModel;

namespace Chindyasov.Vocabulary.Translator.ViewModels
{
    public class ProgressWindowViewModel : WindowViewModelBase
    {
        private double _offsetGreen;
        private double _offsetWhite;
        private bool _allowAnimation;

        public event EventHandler EscButtonPressed;

        public ProgressWindowViewModel(IDimensions dimensions) : base(dimensions)
        {
            Top = dimensions.MainViewHeight;
            IsVisible = false;
            OffsetGreen = 0.0;
            OffsetWhite = 0.0;

            PropertyChanged += HandlePropertyChanged;
        }

        public double OffsetGreen
        {
            get
            {
                return _offsetGreen;
            }
            set
            {
                if (_offsetGreen != value)
                {
                    _offsetGreen = value;
                    NotifyPropertyChanged(nameof(OffsetGreen));
                }
            }
        }

        public double OffsetWhite
        {
            get
            {
                return _offsetWhite;
            }
            set
            {
                if (_offsetWhite != value)
                {
                    _offsetWhite = value;
                    NotifyPropertyChanged(nameof(OffsetWhite));
                }
            }
        }

        private void HandlePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(IsVisible))
            {
                if (IsVisible == true)
                {
                    _allowAnimation = true;
                    new Thread(AnimationLoop).Start();
                }
                else
                {
                    _allowAnimation = false;
                }
            }
        }

        private void AnimationLoop()
        {
            while (_allowAnimation)
            {
                AnimateGreenToWhite();
                AnimateWhiteToGreen();
            }
        }

        private void AnimateGreenToWhite()
        {
            OffsetGreen = -1.0;
            OffsetWhite = 0.0;
            while (OffsetGreen < 1.0)
            {
                IncrementOffsets();
            }
        }

        private void AnimateWhiteToGreen()
        {
            OffsetGreen = 0.0;
            OffsetWhite = -1.0;
            while (OffsetWhite < 1.0)
            {
                IncrementOffsets();
            }
        }

        private void IncrementOffsets()
        {
            OffsetGreen += 0.05;
            OffsetWhite += 0.05;
            Thread.Sleep(20);
        }

        protected override void HandleEscapeKeyPressedAction()
        {
            EscButtonPressed?.Invoke(this, EventArgs.Empty);
        }
    }
}