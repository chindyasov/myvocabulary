﻿using System;

namespace Chindyasov.Vocabulary.Translator.ViewModels
{
    public class PhraseInputViewModel : ViewModelBase
    {
        private readonly string _suggestion = "enter a text to translate";
        private string _text;
        private bool _isSuggestionMode = true;

        public event EventHandler EnterButtonPressed;
        public event EventHandler TextChanged;

        public PhraseInputViewModel()
        {
            Text = _suggestion;
        }

        public Action MouseButtonDownAction => HandleMouseButtonDownAction;
        public Action LostFocusAction => HandleLostFocusAction;
        public Action EnterButtonPressedAction => HandleEnterButtonPressedAction;

        public bool IsSuggestionMode => _isSuggestionMode;

        public string Text
        {
            get
            {
                return _text;
            }
            set
            {
                value = RemoveSuggestionIfNeeded(value);

                if (_text != value)
                {
                    _text = value;
                    NotifyPropertyChanged(nameof(Text));

                    TextChanged?.Invoke(this, EventArgs.Empty);
                }
            }
        }

        public void Reset()
        {
            Text = _suggestion;
            _isSuggestionMode = true;
        }

        private string RemoveSuggestionIfNeeded(string actualText)
        {
            if (_isSuggestionMode == true && actualText != _suggestion)
            {
                var index = actualText.IndexOf(_suggestion);
                if (index != -1)
                { 
                    actualText = actualText.Substring(0, index);
                }
                _isSuggestionMode = false;
            }
            return actualText;
        }

        private void HandleMouseButtonDownAction()
        {
            if (_isSuggestionMode == true)
            {
                _isSuggestionMode = false;
                Text = "";
            }
        }

        private void HandleLostFocusAction()
        {
            if (_isSuggestionMode == false && Text == "")
            {
                _isSuggestionMode = true;
                Text = _suggestion;
            }
        }

        private void HandleEnterButtonPressedAction()
        {
            if (_isSuggestionMode == false)
            {
                EnterButtonPressed?.Invoke(this, EventArgs.Empty);
            }
        }
    }
}
