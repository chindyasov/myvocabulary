﻿using System;

namespace Chindyasov.Vocabulary.Translator.ViewModels
{
    public abstract class WindowViewModelBase : ViewModelBase
    {
        private double _top;
        private double _left;
        private bool _isVisible;
        private IDimensions _dimensions;

        public WindowViewModelBase(IDimensions dimensions)
        {
            _dimensions = dimensions;
            CalculateLeftMargin();
        }

        public Action EscapeKeyPressedAction => HandleEscapeKeyPressedAction;

        public double Top
        {
            get
            {
                return _top;
            }
            set
            {
                if (value != _top)
                {
                    _top = value;
                    NotifyPropertyChanged(nameof(Top));
                }
            }
        }

        public double Left
        {
            get
            {
                return _left;
            }
            set
            {
                if (value != _left)
                {
                    _left = value;
                    NotifyPropertyChanged(nameof(Left));
                }
            }
        }

        public bool IsVisible
        {
            get
            {
                return _isVisible;
            }
            set
            {
                if (_isVisible != value)
                {
                    CalculateLeftMargin();

                    _isVisible = value;
                    NotifyPropertyChanged(nameof(IsVisible));
                }
            }
        }

        protected abstract void HandleEscapeKeyPressedAction();

        private void CalculateLeftMargin()
        {
            Left = (_dimensions.ScreenWidth - _dimensions.MainViewWidth) / 2;
        }
    }
}
