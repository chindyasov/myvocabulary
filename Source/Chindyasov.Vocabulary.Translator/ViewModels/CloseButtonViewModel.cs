﻿using System;

namespace Chindyasov.Vocabulary.Translator.ViewModels
{
    public enum CloseButtonState
    {
        Default,
        Clicked,
        Active
    }

    public class CloseButtonViewModel : ViewModelBase
    {
        private bool _leftButtonPressed;
        private CloseButtonState _buttonState;

        public event EventHandler CloseButtonClick;

        public CloseButtonViewModel()
        {
            _buttonState = CloseButtonState.Default;
        }

        public Action MouseLeaveAction => MouseLeave;
        public Action MouseEnterAction => MouseEnter;
        public Action MouseLeftButtonDownAction => LeftButtonDown;
        public Action MouseLeftButtonUpAction => LeftButtonUp;

        public CloseButtonState ButtonState
        {
            get
            {
                return _buttonState;
            }
            set
            {
                if (_buttonState != value)
                {
                    _buttonState = value;
                    NotifyPropertyChanged(nameof(ButtonState));
                }
            }
        }

        public void LeftButtonDown()
        {
            ButtonState = CloseButtonState.Clicked;
            _leftButtonPressed = true;
        }

        public void LeftButtonUp()
        {
            ButtonState = CloseButtonState.Active;
            if (_leftButtonPressed == true)
            {
                OnCloseButtonClick();
            }
        }

        public void MouseLeave()
        {
            _leftButtonPressed = false;
            ButtonState = CloseButtonState.Default;
        }

        private void MouseEnter()
        {
            ButtonState = CloseButtonState.Active;
        }

        private void OnCloseButtonClick()
        {
            CloseButtonClick?.Invoke(this, EventArgs.Empty);
        }
    }
}
