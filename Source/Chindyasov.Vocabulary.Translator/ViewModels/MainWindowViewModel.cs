﻿using System;
using System.Threading;
using Chindyasov.Utils;
using Chindyasov.Vocabulary.Translator.Models;
using Chindyasov.Vocabulary.Translator.Services;
using Chindyasov.Vocabulary.TranslationProviders;

namespace Chindyasov.Vocabulary.Translator.ViewModels
{
    public class MainWindowViewModel : WindowViewModelBase
    {
        private readonly IDimensions _dimensions;
        private readonly PhraseInputViewModel _phraseInputViewModel;
        private readonly TranslationViewModel _translationViewModel;
        private readonly ProgressWindowViewModel _progressWindowViewModel;
        private readonly MessageViewModel _messageViewModel;
        private readonly UpdateWindowViewModel _updateWindowViewModel;
        private readonly ITranslationLoader _translationLoader;
        private readonly IHotKeyHandler _hotKeyHandler;
        private TranslationDirection _translationDirection;
        private string _title;

        public MainWindowViewModel(IDimensions dimensions,
                                   PhraseInputViewModel phraseInputViewModel,
                                   CloseButtonViewModel closeButtonViewModel,
                                   TranslationViewModel translationViewModel,
                                   ProgressWindowViewModel progressWindowViewModel,
                                   MessageViewModel messageViewModel,
                                   UpdateWindowViewModel updateWindowViewModel,
                                   ITranslationLoader translationLoader,
                                   IHotKeyHandler hotKeyHandler) : base(dimensions)
        {
            _dimensions = dimensions;
            _translationDirection = TranslationDirection.EN_RU;
            _phraseInputViewModel = phraseInputViewModel;
            _translationViewModel = translationViewModel;
            _progressWindowViewModel = progressWindowViewModel;
            _messageViewModel = messageViewModel;
            _translationLoader = translationLoader;
            _updateWindowViewModel = updateWindowViewModel;
            _hotKeyHandler = hotKeyHandler;

            _hotKeyHandler.HotKeyWasDetected += HandleHotKeyWasDetected;
            _translationViewModel.EscButtonPressed += HandleHideAction;
            _progressWindowViewModel.EscButtonPressed += HandleHideAction;
            _messageViewModel.EscButtonPressed += HandleHideAction;
            _updateWindowViewModel.EscButtonPressed += HandleHideAction;
            _updateWindowViewModel.DownloadLinkClicked += HandleHideAction;
            closeButtonViewModel.CloseButtonClick += HandleHideAction;
            phraseInputViewModel.EnterButtonPressed += HandleEnterButtonPressed;
            phraseInputViewModel.TextChanged += HanlePhraseChanged;
        }

        public Action ViewLoadedAction => HandleViewLoadedAction;
        public Action TranslationDirectionClickAction => HandleTranslationDirectionClickAction;

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                if (_title != value)
                {
                    _title = value;
                    NotifyPropertyChanged(nameof(Title));
                }
            }
        }

        public TranslationDirection CurrentTranslationDirection
        {
            get
            {
                return _translationDirection;
            }
            set
            {
                if (_translationDirection != value)
                {
                    _translationDirection = value;
                    NotifyPropertyChanged(nameof(CurrentTranslationDirection));
                }
            }
        }

        public void ShowView()
        {
            _updateWindowViewModel.ShowViewIfNeeded();

            IsVisible = true;

            while (Top < 0)
            {
                Top += 5;
                Thread.Sleep(10);
            }
            Top = 0;
        }

        public void HideView()
        {
            _translationLoader.TranslationLoaded -= HandleTranslationLoaded;
            _translationViewModel.IsVisible = false;
            _progressWindowViewModel.IsVisible = false;
            _messageViewModel.IsVisible = false;

            _updateWindowViewModel.HideView();

            while (Top > -_dimensions.MainViewHeight)
            {
                Top -= 5;
                Thread.Sleep(10);
            }
            Top = -_dimensions.MainViewHeight;
            IsVisible = false;
            _phraseInputViewModel.Reset();
        }

        public void ShowWithStartupState()
        {
            Top = -_dimensions.MainViewHeight;
            IsVisible = true;
        }

        protected override void HandleEscapeKeyPressedAction()
        {
            HideView();
        }

        private void HandleHideAction(object sender, EventArgs e)
        {
            HideView();
        }

        private void HandleViewLoadedAction()
        {
            HideView();
        }

        private void HandleHotKeyWasDetected(object sender, EventArgs e)
        {
            if (IsVisible == false)
            {
                ShowView();
            }
            else
            {
                HideView();
            }
        }

        private void HandleEnterButtonPressed(object sender, EventArgs e)
        {
            _translationViewModel.IsVisible = false;
            _progressWindowViewModel.IsVisible = true;
            _updateWindowViewModel.IsVisible = false;
            _translationLoader.TranslationLoaded += HandleTranslationLoaded;
            _translationLoader.TranslationError += HandleTranslationError;

            _translationLoader.StartForPhrase(_phraseInputViewModel.Text, CurrentTranslationDirection);
        }

        private void HandleTranslationLoaded(object sender, ITranslationResult translationResult)
        {
            _progressWindowViewModel.IsVisible = false;
            _translationViewModel.ShowTranslation(translationResult);
        }

        private void HandleTranslationError(object sender, TranslationException e)
        {
            _messageViewModel.IsVisible = true;
        }
        
        private void HanlePhraseChanged(object sender, EventArgs e)
        {
            if (_progressWindowViewModel.IsVisible == true || _translationViewModel.IsVisible == true || _messageViewModel.IsVisible == true)
            {
                _progressWindowViewModel.IsVisible = false;
                _translationViewModel.IsVisible = false;
                _messageViewModel.IsVisible = false;
                _translationLoader.TranslationLoaded -= HandleTranslationLoaded;
                _translationLoader.TranslationError -= HandleTranslationError;
            }

            DetectTranslationDirection();
        }

        private void HandleTranslationDirectionClickAction()
        {
            if (CurrentTranslationDirection == TranslationDirection.EN_RU)
                CurrentTranslationDirection = TranslationDirection.RU_EN;
            else
                CurrentTranslationDirection = TranslationDirection.EN_RU;
        }

        private void DetectTranslationDirection()
        {
            if (_phraseInputViewModel.IsSuggestionMode)
                return;

            var phraseLanguage = Phrase.DetectLanguage(_phraseInputViewModel.Text);

            if (phraseLanguage == PhraseLanguage.En)
                CurrentTranslationDirection = TranslationDirection.EN_RU;
            else if (phraseLanguage == PhraseLanguage.Ru)
                CurrentTranslationDirection = TranslationDirection.RU_EN;
        }
    }
}
