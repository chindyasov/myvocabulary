﻿using System;

namespace Chindyasov.Vocabulary.Translator.ViewModels
{
    public class MessageViewModel : WindowViewModelBase
    {
        public event EventHandler EscButtonPressed;

        public MessageViewModel(IDimensions dimensions) : base(dimensions)
        {
            Top = dimensions.MainViewHeight;
            IsVisible = false;
        }

        protected override void HandleEscapeKeyPressedAction()
        {
            EscButtonPressed?.Invoke(this, EventArgs.Empty);
        }
    }
}
