﻿using System;
using Chindyasov.Vocabulary.Translator.Services;
using Chindyasov.Vocabulary.TranslationProviders;

namespace Chindyasov.Vocabulary.Translator.ViewModels
{
    public class TranslationViewModel : WindowViewModelBase
    {
        private IDimensions _dimensions;
        private ITranslationResult _translationResult;
        private IBrowserOpener _browserOpener;
        private string _vendorName;
        private string _vendorUrl;

        public event EventHandler EscButtonPressed;

        public TranslationViewModel(IDimensions dimensions, IBrowserOpener browserOpener) : base(dimensions)
        {
            IsVisible = false;
            _dimensions = dimensions;
            Top = _dimensions.MainViewHeight;
            _browserOpener = browserOpener;
        }

        public Action VendorLinkClickedAction => OpenVendorPage;

        public ITranslationResult TranslationResult
        {
            get
            {
                return _translationResult;
            }
            set
            {
                if (_translationResult == value)
                    return;
                _translationResult = value;
                NotifyPropertyChanged(nameof(TranslationResult));
            }
        }

        public string VendorName
        { 
            get
            {
                return _vendorName;
            }
            set
            {
                if (_vendorName == value)
                    return;
                _vendorName = value;
                NotifyPropertyChanged(nameof(VendorName));
            }
        }

        public string VendorUrl
        {
            get
            {
                return _vendorUrl;
            }
            set
            {
                if (_vendorUrl == value)
                    return;
                _vendorUrl = value;
                NotifyPropertyChanged(nameof(VendorUrl));
            }
        }

        public void ShowTranslation(ITranslationResult translation)
        {
            TranslationResult = translation;
            VendorName = translation.VendorName;
            VendorUrl = translation.VendorUrl;
            IsVisible = true;
        }

        protected override void HandleEscapeKeyPressedAction()
        {
            EscButtonPressed?.Invoke(this, EventArgs.Empty);
        }

        private void OpenVendorPage()
        {
            _browserOpener?.Open(VendorUrl);
        }
    }
}
