﻿namespace Chindyasov.Vocabulary.Translator.Services
{
    public interface IBrowserOpener
    {
        void Open(string url);
    }
}
