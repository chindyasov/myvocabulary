﻿using System;
using System.Threading;
using System.Windows.Interop;
using System.Runtime.InteropServices;

namespace Chindyasov.Vocabulary.Translator.Services
{
    class HotKeyHandler : IHotKeyHandler
    {
        private IntPtr _hwnd;

        private readonly int KEY_T = 84;
        private readonly int KEY_CTRL = 2;
        private readonly int WM_HOTKEY = 0x0312;

        public event EventHandler HotKeyWasDetected;

        public void RegisterForHandle(IntPtr hwnd)
        {
            if (_hwnd != IntPtr.Zero)
            {
                throw new InvalidOperationException("this handler already in use");
            }

            var wndSource = HwndSource.FromHwnd(hwnd);

            if (wndSource == null)
            {
                throw new InvalidOperationException("window was not found for given handle");
            }

            wndSource.AddHook(WindowHook);

            _hwnd = hwnd;

            var res = RegisterHotKey(_hwnd, 0, KEY_CTRL, KEY_T);
        }

        ~HotKeyHandler()
        {
            UnregisterHotKey(_hwnd, 0);
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vk);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        private IntPtr WindowHook(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (_hwnd == hwnd && msg == WM_HOTKEY)
            {
                new Thread(OnHotKeyDetected).Start();
                handled = true;
            }
            return IntPtr.Zero;
        }

        private void OnHotKeyDetected()
        {
            HotKeyWasDetected?.Invoke(this, EventArgs.Empty);
        }
    }
}
