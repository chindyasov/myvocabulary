﻿using System;

namespace Chindyasov.Vocabulary.Translator.Services
{
    public interface IHotKeyHandler
    {
        event EventHandler HotKeyWasDetected;
    }
}
