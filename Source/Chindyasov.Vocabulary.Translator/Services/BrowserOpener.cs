﻿using System.Diagnostics;

namespace Chindyasov.Vocabulary.Translator.Services
{
    class BrowserOpener : IBrowserOpener
    {
        public void Open(string url)
        {
            Process.Start(url);
        }
    }
}
