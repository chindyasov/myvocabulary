﻿using System;
using System.Collections;
using System.Text;

namespace Chindyasov.Utils
{
    public enum PhraseType { Empty, SingleWord, MultiWord };
    public enum PhraseLanguage { Unknown, En, Ru };

    public class Phrase
    {
        private static readonly ArrayList punctuationCharacters = new ArrayList { '!', '?', '.', ',' };

        public static PhraseType DetectPhraseType(string str)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return PhraseType.Empty;
            }
            str = str.Trim();
            if (str.Contains(" "))
            {
                return PhraseType.MultiWord;
            }
            return PhraseType.SingleWord;
        }

        public static string Normalize(string str)
        {
            if (DetectPhraseType(str) == PhraseType.Empty)
                return string.Empty;

            var subStrings = str.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
            var result = new StringBuilder(subStrings[0]);
            for (var i = 1; i < subStrings.Length; i++)
            {
                if (!punctuationCharacters.Contains(subStrings[i][0]))
                    result.Append(" ");
                result.Append(subStrings[i]);
            }
            return result.ToString();
        }

        public static PhraseLanguage DetectLanguage(string phrase)
        {
            if (phrase == null)
                return PhraseLanguage.Unknown;

            phrase = phrase.ToLower(new System.Globalization.CultureInfo("ru-RU"));

            var enSymbolsCount = 0;
            var ruSymbolsCount = 0;

            foreach (var symbol in phrase)
            {
                var symbolLanguage = GetSymbolLanguage(symbol);

                if (symbolLanguage == PhraseLanguage.En)
                    enSymbolsCount++;
                else if (symbolLanguage == PhraseLanguage.Ru)
                    ruSymbolsCount++;
            }

            if (enSymbolsCount == 0 && ruSymbolsCount == 0)
                return PhraseLanguage.Unknown;
            else if ((enSymbolsCount > ruSymbolsCount) || (enSymbolsCount == ruSymbolsCount))
                return PhraseLanguage.En;
            else if (ruSymbolsCount > enSymbolsCount)
                return PhraseLanguage.Ru;
            else
                return PhraseLanguage.Unknown;
        }

        private static PhraseLanguage GetSymbolLanguage(char sym)
        {
            var symCode = (int)sym;

            if (symCode >= 97 && symCode <= 122)
                return PhraseLanguage.En;
            else if (symCode >= 1072 && symCode <= 1103 || symCode == 1105)
                return PhraseLanguage.Ru;
            else
                return PhraseLanguage.Unknown;
        }
    }
}