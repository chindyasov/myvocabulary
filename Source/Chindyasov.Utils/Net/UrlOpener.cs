﻿using System;
using System.IO;
using System.Net;

namespace Chindyasov.Utils.Net
{
    public class UrlOpener : IUrlOpener
    {
        public string ReadToEnd(string url)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(new Uri(url));
                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    using (var sr = new StreamReader(response.GetResponseStream()))
                    {
                        return sr.ReadToEnd();
                    }
                }
            }
            catch (SystemException exception)
            {
                throw new UrlOperationException(exception);
            }
        }
    }
}