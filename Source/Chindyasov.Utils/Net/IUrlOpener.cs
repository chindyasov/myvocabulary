﻿namespace Chindyasov.Utils.Net
{
    public interface IUrlOpener
    {
        string ReadToEnd(string url);
    }
}
