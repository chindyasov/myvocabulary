﻿using System;

namespace Chindyasov.Utils.Net
{
    public class UrlOperationException : Exception
    {
        public UrlOperationException() : base()
        {
        }

        public UrlOperationException(Exception innerException) : base("url operation error", innerException)
        {
        }
    }
}
