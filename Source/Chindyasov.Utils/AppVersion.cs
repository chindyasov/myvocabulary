﻿using System;

namespace Chindyasov.Utils
{
    public struct AppVersion
    {
        public int Major;
        public int Minor;
        public int Revision;

        public AppVersion(int major, int minor, int revision)
        {
            Major = major;
            Minor = minor;
            Revision = revision;
        }

        public AppVersion(string version)
        {
            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentException("empty version string was given");

            var components = version.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);

            try
            {
                Major = int.Parse(components[0]);
                Minor = int.Parse(components[1]);
                Revision = int.Parse(components[2]);
            }
            catch (Exception e)
            {
                throw new ArgumentException("version string format is illegal", e);
            }
        }

        public static bool operator ==(AppVersion v1, AppVersion v2) => v1.Equals(v2);

        public static bool operator !=(AppVersion v1, AppVersion v2) => !v1.Equals(v2);

        public static bool operator <(AppVersion v1, AppVersion v2) => !(v1 > v2) && !(v1 == v2);

        public static bool operator >(AppVersion v1, AppVersion v2)
        {
            return v1.Major > v2.Major
                || (v1.Major == v2.Major && v1.Minor > v2.Minor)
                || (v1.Major == v2.Major && v1.Minor == v2.Minor && v1.Revision > v2.Revision);
        }

        public override int GetHashCode() => 100 * Major + 10 * Minor + Revision;

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (GetType() != obj.GetType())
                return false;

            var o = (AppVersion)obj;

            return Major == o.Major && Minor == o.Minor && Revision == o.Revision;
        }

        private bool IsGreaterOf(AppVersion version)
        {
            return Major > version.Major || Minor > version.Minor || Revision > version.Revision;
        }
    }
}
