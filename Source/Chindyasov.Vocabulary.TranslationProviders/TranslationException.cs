﻿using System;

namespace Chindyasov.Vocabulary.TranslationProviders
{
    public class TranslationException : Exception
    {
        private string _phrase;
        private string _reason;

        public TranslationException(string phrase, string reason, Exception exception = null) : base(reason, exception)
        {
            _phrase = phrase;
            _reason = reason;
        }

        public TranslationException(string phrase, Exception exception = null) : this(phrase, string.Empty, exception)
        {
        }

        public string Phrase => _phrase;
        public string Reason => _reason;
    }
}
