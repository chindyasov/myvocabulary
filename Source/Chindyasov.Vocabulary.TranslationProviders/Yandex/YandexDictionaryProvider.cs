﻿using Chindyasov.Utils;
using Chindyasov.Utils.Net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Chindyasov.Vocabulary.TranslationProviders.Yandex
{
    public class YandexDictionaryProvider : IDictionaryProvider
    {
        private readonly string _urlTemplate = "https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key={0}&ui=ru&text={1}&lang={2}";
        private readonly string _vendorName = "Yandex.Dictionary";
        private readonly string _vendorUrl = "http://api.yandex.com/dictionary";
        private readonly string _apiKey;

        private IUrlOpener _urlOpener;

        public YandexDictionaryProvider(IUrlOpener urlOpener)
        {
            _urlOpener = urlOpener;
            _apiKey = YandexApiKeyProvider.DictionaryApiKey;
        }

        public IDictionaryTranslationResult Translate(string phrase, TranslationDirection direction)
        {
            ValidatePhrase(phrase);
            var response = GetServerResponse(phrase, direction);
            var dictionaryArticles = ExtractDictionaryArticlesFromResponse(phrase, response);

            return new DictionaryTranslationResult(dictionaryArticles, _vendorName, _vendorUrl);
        }

        private void ValidatePhrase(string phrase)
        {
            var phraseType = Phrase.DetectPhraseType(phrase);
            if (phraseType == PhraseType.MultiWord || phraseType == PhraseType.Empty)
                throw new TranslationException(phrase, "multiword phrase was given");
        }

        private string GetServerResponse(string phrase, TranslationDirection direction)
        {
            try
            {
                return _urlOpener.ReadToEnd(string.Format(_urlTemplate, _apiKey, phrase, TranslationDirectionConverter.ToString(direction)));
            }
            catch (Exception e)
            {
                throw new TranslationException(phrase, e);
            }
        }

        private IReadOnlyCollection<IDictionaryArticle> ExtractDictionaryArticlesFromResponse(string phrase, string response)
        {
            try
            {
                var jsonArticles = GetJsonArticles(phrase, response);
                return GetTranslationArticlesFromJson(jsonArticles, phrase);
            }
            catch (Exception e)
            {
                throw new TranslationException(phrase, e);
            }
        }

        private JsonArticles GetJsonArticles(string phrase, string serverResponse)
        {
            var jsonArticles = JsonConvert.DeserializeObject<JsonArticles>(serverResponse);
            if (jsonArticles.ArticlesList == null || jsonArticles.ArticlesList.Count == 0)
                throw new TranslationException(phrase, "invalid JSON structure");
            return jsonArticles;
        }

        private IReadOnlyCollection<IDictionaryArticle> GetTranslationArticlesFromJson(JsonArticles jsonArticles, string phrase)
        {
            var dictionaryArticles = new List<DictionaryArticle>();

            foreach (var jsonArticle in jsonArticles.ArticlesList)
            {
                if (jsonArticle.TranslationsList == null || jsonArticle.TranslationsList.Count == 0)
                    throw new TranslationException(phrase, "invalid JSON structure");

                var translationVariants = GetTranslationVariantsFromJsonArticle(jsonArticle, phrase);

                dictionaryArticles.Add(new DictionaryArticle(jsonArticle.PartOfSpeech, jsonArticle.Transcription ?? string.Empty, translationVariants));
            }

            return dictionaryArticles;
        }

        private List<string> GetTranslationVariantsFromJsonArticle(JsonTranslations jsonArticle, string phrase)
        {
            var translationVariants = new List<string>();

            foreach (var translationVariant in jsonArticle.TranslationsList)
            {
                if (string.IsNullOrWhiteSpace(translationVariant.Text))
                    throw new TranslationException(phrase, "invalid JSON structure");

                translationVariants.Add(translationVariant.Text);
            }

            return translationVariants;
        }
    }

    class JsonArticles
    {
        [JsonProperty("def", Required = Required.Always)]
        public List<JsonTranslations> ArticlesList { get; set; }
    }

    class JsonTranslations
    {
        [JsonProperty("pos", Required = Required.Always)]
        public string PartOfSpeech { get; set; }

        [JsonProperty("ts")]
        public string Transcription { get; set; }

        [JsonProperty("tr", Required = Required.Always)]
        public List<JsonTranslationEntry> TranslationsList { get; set; }
    }

    class JsonTranslationEntry
    {
        [JsonProperty("text", Required = Required.Always)]
        public string Text { get; set; }
    }
}
