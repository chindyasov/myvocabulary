﻿// Please use your own Yandex API keys.
//
// Just replase <TranslateApiKey> and <DictionaryApiKey> in a
// code below. You can get the API keys by link:
//
// https://tech.yandex.com/

namespace Chindyasov.Vocabulary.TranslationProviders.Yandex
{
    class YandexApiKeyProvider
    {
        public static string TranslateApiKey => "<TranslateApiKey>";
        public static string DictionaryApiKey => "<DictionaryApiKey>";
    }
}
