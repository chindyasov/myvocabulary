﻿using System;

static class TranslationDirectionConverter
{
    public static string ToString(TranslationDirection direction)
    {
        switch (direction)
        {
            case TranslationDirection.EN_RU:
                return "en-ru";
            case TranslationDirection.RU_EN:
                return "ru-en";
            default:
                throw new ArgumentException("invalid translation direction");
        }
    }
}