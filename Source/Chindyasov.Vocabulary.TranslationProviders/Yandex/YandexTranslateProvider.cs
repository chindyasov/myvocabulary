﻿using Chindyasov.Utils;
using Chindyasov.Utils.Net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Chindyasov.Vocabulary.TranslationProviders.Yandex
{
    public class YandexTranslateProvider : ITranslationProvider
    {
        private readonly string _urlTemplate = "https://translate.yandex.net/api/v1.5/tr.json/translate?key={0}&text={1}&lang={2}";
        private readonly string _vendorName = "Yandex.Translate";
        private readonly string _vendorUrl = "http://translate.yandex.com/";
        private readonly string _apiKey;

        private IUrlOpener _urlOpener;

        public YandexTranslateProvider(IUrlOpener urlOpener)
        {
            _urlOpener = urlOpener;
            _apiKey = YandexApiKeyProvider.TranslateApiKey;
        }

        public ITranslationResult Translate(string phrase, TranslationDirection direction)
        {
            if (Phrase.DetectPhraseType(phrase) == PhraseType.Empty)
                throw new TranslationException(phrase, "phrase is empty");

            phrase = Phrase.Normalize(phrase);
            var response = GetServerResponse(phrase, direction);
            return new TranslatiaonResult(ExtractTranslationFromResponse(phrase, response), _vendorName, _vendorUrl);
        }

        private string GetServerResponse(string phrase, TranslationDirection direction)
        {
            try
            {
                var url = String.Format(_urlTemplate, _apiKey, phrase, TranslationDirectionConverter.ToString(direction));
                return _urlOpener.ReadToEnd(url);
            }
            catch (Exception e)
            {
                throw new TranslationException(phrase, e);
            }
        }

        private string ExtractTranslationFromResponse(string phrase, string responseJSON)
        {
            try
            {
                var response = DeserializeServerResponse(responseJSON, phrase);
                return GetTranslationFromResponse(response, phrase);
            }
            catch (Exception e)
            {
                throw new TranslationException(phrase, e);
            }
        }

        private ServerResponse DeserializeServerResponse(string responseJSON, string phrase)
        {
            ServerResponse response = JsonConvert.DeserializeObject<ServerResponse>(responseJSON);

            if (response.StatusCode != 200 || response.Translations == null || response.Translations.Count != 1)
                throw new TranslationException(phrase, "JSON is not valid");

            return response;
        }

        private string GetTranslationFromResponse(ServerResponse response, string phrase)
        {
            var translation = response.Translations[0];

            if (string.IsNullOrWhiteSpace(translation))
                throw new TranslationException(phrase, "empty translation was given");

            if (phrase.Equals(translation))
                throw new TranslationException(phrase, "translation result is equal to phrase");

            return translation;
        }
    }

    internal class ServerResponse
    {
        [JsonProperty("code", Required = Required.Always)]
        public int StatusCode { get; set; }

        [JsonProperty("text", Required = Required.Always)]
        public List<string> Translations { get; set; }
    }
}
