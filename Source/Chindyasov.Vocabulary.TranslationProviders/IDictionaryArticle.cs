﻿using System.Collections.Generic;

namespace Chindyasov.Vocabulary.TranslationProviders
{
    public interface IDictionaryArticle
    {
        string PartOfSpeech { get; }
        string Transcription { get; }
        IEnumerable<string> TranslationVariants { get; }
    }
}
