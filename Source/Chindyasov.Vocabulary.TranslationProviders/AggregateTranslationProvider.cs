﻿using Chindyasov.Utils;

namespace Chindyasov.Vocabulary.TranslationProviders
{
    public class AggregateTranslationProvider : ITranslationProvider
    {
        private readonly IDictionaryProvider _dictionaryProvider;
        private readonly ITranslationProvider _translationProvider;

        public AggregateTranslationProvider(IDictionaryProvider dictionaryProvider, ITranslationProvider translationProvider)
        {
            _dictionaryProvider = dictionaryProvider;
            _translationProvider = translationProvider;
        }

        public ITranslationResult Translate(string phrase, TranslationDirection direction)
        {
            return Phrase.DetectPhraseType(phrase) == PhraseType.SingleWord ? TranslateSinglewordPhrase(phrase, direction) 
                                                                            : _translationProvider.Translate(phrase, direction);
        }

        private ITranslationResult TranslateSinglewordPhrase(string phrase, TranslationDirection direction)
        {
            try
            {
                return _dictionaryProvider.Translate(phrase, direction);
            }
            catch (TranslationException)
            {
                return _translationProvider.Translate(phrase, direction);
            }
        }
    }
}
