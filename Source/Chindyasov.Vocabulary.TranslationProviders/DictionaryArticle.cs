﻿using System.Collections.Generic;

namespace Chindyasov.Vocabulary.TranslationProviders
{
    class DictionaryArticle : IDictionaryArticle
    {
        public DictionaryArticle(string partOfSpeech, string transcription, IEnumerable<string> translationVariants)
        {
            PartOfSpeech = partOfSpeech;
            Transcription = transcription;
            TranslationVariants = translationVariants;
        }

        public string PartOfSpeech { get; private set; }
        public string Transcription { get; private set; }
        public IEnumerable<string> TranslationVariants { get; private set; }
    }
}
