﻿using System.Collections.Generic;
using System.Text;

namespace Chindyasov.Vocabulary.TranslationProviders
{
    public class DictionaryTranslationConverter
    {
        public static string ConvertToString(IEnumerable<IDictionaryArticle> articles)
        {
            StringBuilder sb = new StringBuilder();
            var isFirst = true;

            foreach (var article in articles)
            {
                foreach (var translation in article.TranslationVariants)
                {
                    if (!isFirst)
                        sb.Append("\n");
                    else
                        isFirst = false;

                    sb.Append(translation);
                }
            }
            return sb.ToString();
        }

        public static List<IDictionaryArticle> ConvertToList(IEnumerable<IDictionaryArticle> articles)
        {
            return new List<IDictionaryArticle>(articles);
        }
    }
}
