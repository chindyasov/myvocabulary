﻿namespace Chindyasov.Vocabulary.TranslationProviders
{
    public interface ITranslationResult
    {
        string Translation { get; }
        string VendorName { get; }
        string VendorUrl { get; }
    }
}
