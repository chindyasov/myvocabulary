﻿namespace Chindyasov.Vocabulary.TranslationProviders
{
    public interface ITranslationProvider
    {
        ITranslationResult Translate(string phrase, TranslationDirection direction);
    }
}
