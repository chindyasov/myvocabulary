﻿namespace Chindyasov.Vocabulary.TranslationProviders
{
    public interface IDictionaryProvider
    {
        IDictionaryTranslationResult Translate(string phrase, TranslationDirection direction);
    }
}
