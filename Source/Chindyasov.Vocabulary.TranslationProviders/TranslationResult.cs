﻿namespace Chindyasov.Vocabulary.TranslationProviders
{
    public class TranslatiaonResult : ITranslationResult
    {
        public TranslatiaonResult(string translation, string vendorName, string vendorUrl)
        {
            Translation = translation;
            VendorName = vendorName;
            VendorUrl = vendorUrl;
        }

        public string Translation { get; private set; }
        public string VendorName { get; private set; }
        public string VendorUrl { get; private set; }
    }
}
