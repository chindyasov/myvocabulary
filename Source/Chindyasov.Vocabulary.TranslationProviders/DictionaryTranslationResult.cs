﻿using System.Collections.Generic;

namespace Chindyasov.Vocabulary.TranslationProviders
{
    public class DictionaryTranslationResult : IDictionaryTranslationResult
    {
        public DictionaryTranslationResult(IReadOnlyCollection<IDictionaryArticle> dictionaryArticles, string vendorName, string vendorUrl)
        {
            DictionaryArticles = dictionaryArticles;
            Translation = DictionaryTranslationConverter.ConvertToString(dictionaryArticles);
            VendorName = vendorName;
            VendorUrl = vendorUrl;
        }

        public string Translation { get; private set; }
        public IReadOnlyCollection<IDictionaryArticle> DictionaryArticles { get; private set; }
        public string VendorName { get; private set; }
        public string VendorUrl { get; private set; }
    }
}
