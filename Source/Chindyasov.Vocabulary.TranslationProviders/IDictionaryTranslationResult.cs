﻿using System.Collections.Generic;

namespace Chindyasov.Vocabulary.TranslationProviders
{
    public interface IDictionaryTranslationResult : ITranslationResult
    {
        IReadOnlyCollection<IDictionaryArticle> DictionaryArticles { get; }
    }
}
