MyVocabulary Project Documentation
==================================

Author info
-----------

Maxim Chindyasov / [max@chindyasov.ru](mailto:max@chindyasov.ru "email")

License info
------------

To read a license see file `<ROOT_PROJECT_FOLDER>/LICENSE.md`.

Before you start
-----------------

To use all features of the project you need to retrieve Yandex Translation
and Dictionary API keys. You can do this action for free on the
[Yandex Technologies page](https://tech.yandex.com/ "Yandex Technologies").

You need to insert keys to correspond positions in file
`<ROOT_PROJECT_FOLDER>/Source/Chindyasov.Vocabulary.TranslationProviders/Yandex/YandexApiKeyProvider.cs`

Project structure
-----------------

To open the project in Microsoft Visual Studio open `MyVocabulary.sln` file
from the root project folder. The solution includes several C# projects:

1. **Chindyasov.Utils**: some common useful classes.
2. **Chindyasov.Vocabulary.TranslationProviders**: translation infrastructure.
3. **Chindyasov.Vocabulary.Translator**: main application module.
4. **IntegrationTests** and **UnitTests**.

Testing
-------

To test the project use your favourite **NUnit-2.6.4**-compatible test runner
for **IntegrationTests** and **UnitTests** projects.

Installer
---------------

Build solution in `Release` configuration, then open file
`<ROOT_PROJECT_FOLDER>/Installer/installer.iss` in `Inno Setup 5.5.8 (u)`.
