[Setup]
AppName=MyVocabulary Translator
AppVersion=1.0
AppPublisher=Maxim Chindyasov
AppPublisherURL=http://chindyasov.ru
DefaultDirName={pf}\Chindyasov\MyVocabulary\Translator
OutputDir=output
OutputBaseFilename=SetupMyVocabularyTranslator
DisableWelcomePage=no
RestartApplications=no
SetupIconFile=..\Source\Chindyasov.Vocabulary.Translator\Resources\AppIcon.ico
LicenseFile=..\LICENSE.md
UninstallFilesDir={app}\uninst

[Files]
Source: "..\Source\Chindyasov.Vocabulary.Translator\bin\Release\Autofac.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Source\Chindyasov.Vocabulary.Translator\bin\Release\Chindyasov.Utils.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Source\Chindyasov.Vocabulary.Translator\bin\Release\Chindyasov.Vocabulary.TranslationProviders.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Source\Chindyasov.Vocabulary.Translator\bin\Release\MyVocabulary Translator.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Source\Chindyasov.Vocabulary.Translator\bin\Release\Newtonsoft.Json.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\LICENSE.md"; DestDir: "{app}"; Flags: ignoreversion

[Registry]
Root: HKCU; Subkey: "Software\Microsoft\Windows\CurrentVersion\Run"; ValueType: string; ValueName: "MyVocabulary Translator"; ValueData: "{app}\MyVocabulary Translator.exe"; Flags: deletekey uninsdeletevalue

[Run]
Filename: "{app}\MyVocabulary Translator.exe"; Description: "run the program"; Flags: postinstall nowait

[Code]

#include 'tools.iss'

procedure CurStepChanged(CurStep: TSetupStep);
begin
  if CurStep=ssInstall then
  begin
    StopRunningApplication();
  end;
end;

procedure CurUninstallStepChanged(CurUninstallStep: TUninstallStep);
begin
  if CurUninstallStep=usUninstall then
  begin
    StopRunningApplication();
  end;
end;

procedure InitializeWizard();
begin
  WizardForm.LicenseMemo.Text := LoadLicenseText();
end;
