[Code]

procedure StopRunningApplication();
var
  ResultCode: Integer;
begin
  Exec(ExpandConstant('taskkill.exe'), '/f /im ' + '"' + 'MyVocabulary Translator.exe' + '"', '', SW_HIDE, ewWaitUntilTerminated, ResultCode);
  Sleep(5000);
end;

function LoadLicenseText(): String;
var
  licenseRawText: AnsiString;
  licenseText: String;
begin
  licenseText := 'Fail to obtain a license text. Please download license file from here: https://bitbucket.org/chindyasov/myvocabulary/downloads/LICENSE.md'
  
  ExtractTemporaryFile('LICENSE.md');
  
  if LoadStringFromFile(ExpandConstant('{tmp}\LICENSE.md'), licenseRawText) then
  begin
    licenseText := String(licenseRawText);

    { Remove redundant new line symbols }
    StringChangeEx(licenseText, #13#10#13#10, '<tmp paragraph separator>', True);
    StringChangeEx(licenseText, #13#10, ' ', True);
    StringChangeEx(licenseText, '<tmp paragraph separator>', #13#10#13#10, True);
  end;

  Result := licenseText;
end;
