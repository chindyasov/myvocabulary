﻿using Chindyasov.Utils;
using NUnit.Framework;
using System;

namespace UnitTests
{
    [TestFixture]
    public class AppVersionShould
    {
        [Test]
        public void ParseValidVersionString()
        {
            var version = new AppVersion("1.3.6");

            Assert.That(version.Major, Is.EqualTo(1));
            Assert.That(version.Minor, Is.EqualTo(3));
            Assert.That(version.Revision, Is.EqualTo(6));
        }

        [Test]
        public void UseFirstThreeDigitsFromVersionString()
        {
            var version = new AppVersion("1.2.3.45");

            Assert.That(version.Major, Is.EqualTo(1));
            Assert.That(version.Minor, Is.EqualTo(2));
            Assert.That(version.Revision, Is.EqualTo(3));
        }

        [Test]
        public void DetectEgualVersion()
        {
            var version1 = new AppVersion("1.2.3");
            var version2 = new AppVersion("1.2.3");
            Assert.True(version1 == version2);
        }

        [Test]
        public void DetectNotEqualVersion()
        {
            var version1 = new AppVersion("1.2.3");
            var version2 = new AppVersion("1.2.4");
            Assert.True(version1 != version2);
        }

        [TestCase("1.2.2")]
        [TestCase("1.1.4")]
        [TestCase("0.3.4")]
        public void DetectGreaterVersion(string version)
        {
            var version1 = new AppVersion("1.2.3");
            var version2 = new AppVersion(version);
            Assert.True(version1 > version2);
        }

        [TestCase("0.2.4")]
        [TestCase("1.0.4")]
        [TestCase("1.2.3")]
        public void NotDetectGreaterVersion(string version)
        {
            var version1 = new AppVersion(version);
            var version2 = new AppVersion("1.2.3");
            Assert.False(version1 > version2);
        }

        [TestCase("1.2.4")]
        [TestCase("1.3.0")]
        [TestCase("2.0.0")]
        public void DetectLoverVersion(string version)
        {
            var version1 = new AppVersion("1.2.3");
            var version2 = new AppVersion(version);
            Assert.True(version1 < version2);
        }

        [TestCase("2.2.0")]
        [TestCase("1.3.0")]
        [TestCase("1.2.3")]
        public void NotDetectLoverVersion(string version)
        {
            var version1 = new AppVersion(version);
            var version2 = new AppVersion("1.2.3");
            Assert.False(version1 < version2);
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase("   ")]
        [TestCase("1")]
        [TestCase("1.1")]
        [TestCase("a.b.2.c")]
        [ExpectedException(typeof(ArgumentException))]
        public void ThrowExceptionIfVersionStringIsIncorrect(string versionString)
        {
            var version = new AppVersion(versionString);
        }
    }
}
