﻿using Chindyasov.Vocabulary.TranslationProviders;
using NUnit.Framework;
using UnitTests.Fakes;
using UnitTests.Support;

namespace UnitTests.TranslationProvidersTests
{
    [TestFixture]
    class YandexDictionaryProviderShould
    {
        [Test]
        public void ThrowExceptionIfMultiwordPhraseWasGiven()
        {
            var provider = Create.YandexDictionaryProvider.WithUrlOpener(new CustomUrlOpener("")).Please();
            try
            {
                provider.Translate("hello world", TranslationDirection.EN_RU);
            }
            catch (TranslationException)
            {
                Assert.Pass();
            }
            Assert.Fail("translation provider must throw TranslationException if multiword phrase was given");
        }

        [Test]
        public void AcceptAnySinglewordPhrase()
        {
            var urlOpener = new CustomUrlOpener("{\"head\": {}, \"def\": [{\"pos\": \"noun\", \"tr\": [{\"text\": \"фраза\"}]}]}");
            var provider = Create.YandexDictionaryProvider.WithUrlOpener(urlOpener).Please();
            var translation = provider.Translate(" phrase  ", TranslationDirection.EN_RU).Translation;
            Assert.That(translation, Is.EqualTo("фраза"));
        }

        [TestCase("{\"head\": {}, \"def\": [{\"pos\": \"noun\", \"tr\": [{\"text\": \"translation 1\"}, {\"text\": \"translation 2\"}, {\"text\": \"translation 3\"}]}]}")]
        [TestCase("{\"head\": {}, \"def\": [{\"pos\": \"noun\", \"tr\": [{\"text\": \"translation 1\"}]}, {\"pos\": \"noun\", \"tr\": [{\"text\": \"translation 2\"}]}, {\"pos\": \"noun\", \"tr\": [{\"text\": \"translation 3\"}]}]}")]
        [TestCase("{\"head\": {}, \"def\": [{\"pos\": \"noun\", \"tr\": [{\"text\": \"translation 1\"}]}, {\"pos\": \"noun\", \"tr\": [{\"text\": \"translation 2\"}, {\"text\": \"translation 3\"}]}]}")]
        public void ReturnNewLineSeparatedListOfTranslations(string serverResponse)
        {
            var provider = Create.YandexDictionaryProvider.WithUrlOpener(new CustomUrlOpener(serverResponse)).Please();
            var translation = provider.Translate(" phrase  ", TranslationDirection.EN_RU).Translation;
            Assert.That(translation, Is.EqualTo("translation 1\ntranslation 2\ntranslation 3"));
        }

        [TestCase("{\"head\": {}, \"def\": [{\"pos\": \"noun\", \"tr\": [{\"text\": \"translation 1\"}, {\"text\": \"translation 2\"}, {\"text\": \"translation 3\"}]}]}")]
        [TestCase("{\"head\": {}, \"def\": [{\"pos\": \"noun\", \"tr\": [{\"text\": \"translation 1\"}]}, {\"pos\": \"noun\", \"tr\": [{\"text\": \"translation 2\"}]}, {\"pos\": \"noun\", \"tr\": [{\"text\": \"translation 3\"}]}]}")]
        [TestCase("{\"head\": {}, \"def\": [{\"pos\": \"noun\", \"tr\": [{\"text\": \"translation 1\"}]}, {\"pos\": \"noun\", \"tr\": [{\"text\": \"translation 2\"}, {\"text\": \"translation 3\"}]}]}")]
        public void ProvidePartOfSpeechForDictionaryArticles(string serverResponse)
        {
            var provider = Create.YandexDictionaryProvider.WithUrlOpener(new CustomUrlOpener(serverResponse)).Please();
            var translation = provider.Translate("phrase", TranslationDirection.EN_RU);

            foreach (var article in translation.DictionaryArticles)
            {
                Assert.That(article.PartOfSpeech, Is.EqualTo("noun"));
            }
        }

        [Test]
        public void ProvideTranscriptionForDictionaryArticle()
        {
            var urlOpener = new CustomUrlOpener("{\"head\": {}, \"def\": [{\"pos\": \"noun\", \"ts\": \"transcription\", \"tr\": [{\"text\": \"фраза\"}]}]}");
            var provider = Create.YandexDictionaryProvider.WithUrlOpener(urlOpener).Please();
            var translation = DictionaryTranslationConverter.ConvertToList(provider.Translate(" phrase  ", TranslationDirection.EN_RU).DictionaryArticles);
            Assert.That(translation[0].Transcription, Is.EqualTo("transcription"));
        }

        [Test]
        public void ProvideEmptyStringInsteadOfTranscriptionIfItIsNotPresented()
        {
            var urlOpener = new CustomUrlOpener("{\"head\": {}, \"def\": [{\"pos\": \"noun\", \"tr\": [{\"text\": \"фраза\"}]}]}");
            var provider = Create.YandexDictionaryProvider.WithUrlOpener(urlOpener).Please();
            var translation = DictionaryTranslationConverter.ConvertToList(provider.Translate(" phrase  ", TranslationDirection.EN_RU).DictionaryArticles);
            Assert.That(translation[0].Transcription, Is.EqualTo(string.Empty));
        }

        [TestCase("{\"head\": {}}")]
        [TestCase("{\"head\": {}, \"def\": []}")]
        [TestCase("{\"head\": {}, \"def\": [{}]}")]
        [TestCase("{\"head\": {}, \"def\": [{\"tr\": []}]}")]
        [TestCase("{\"head\": {}, \"def\": [{\"tr\": [{}]}]}")]
        [TestCase("{\"head\": {}, \"def\": [{\"tr\": [{\"text\": \"\"}]}]}")]
        [TestCase("{\"head\": {}, \"def\": [{\"tr\": [{\"text\": \"     \"}]}]}")]
        public void ThrowExceptionIfJsonDoesNotContainsTranslation(string json)
        {
            var provider = Create.YandexDictionaryProvider.WithUrlOpener(new CustomUrlOpener(json)).Please();
            try
            {
                provider.Translate("time", TranslationDirection.EN_RU);
            }
            catch (TranslationException)
            {
                Assert.Pass();
            }
            Assert.Fail("translation provider must throw TranslationException if phrase is empty");
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        [TestCase("   ")]
        public void ThrowExceptionIfPhraseIsEmpty(string phrase)
        {
            var provider = Create.YandexDictionaryProvider.WithUrlOpener(new CustomUrlOpener("")).Please();
            try
            {
                provider.Translate(phrase, TranslationDirection.EN_RU);
            }
            catch (TranslationException)
            {
                Assert.Pass();
            }
            Assert.Fail("translation provider must throw TranslationException if phrase is empty");
        }

        [Test]
        public void ThrowExceptionOnNetworkError()
        {
            var provider = Create.YandexDictionaryProvider.WithUrlOpener(new UrlOpenerWithException()).Please();
            try
            {
                provider.Translate("hello", TranslationDirection.EN_RU);
            }
            catch (TranslationException)
            {
                Assert.Pass();
            }
            Assert.Fail("translation provider must throw TranslationException on network error");
        }

        [Test]
        public void ThrowExceptionIfServerResponceIsNotValidJSON()
        {
            var provider = Create.YandexDictionaryProvider.WithUrlOpener(new CustomUrlOpener("<html></html>")).Please();
            try
            {
                provider.Translate("hello", TranslationDirection.EN_RU);
            }
            catch (TranslationException)
            {
                Assert.Pass();
            }
            Assert.Fail("translation provider must throw TranslationException on invalid server responce");
        }
    }
}
