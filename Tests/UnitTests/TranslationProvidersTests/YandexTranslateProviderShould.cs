﻿using NUnit.Framework;
using Chindyasov.Vocabulary.TranslationProviders;
using UnitTests.Fakes;
using UnitTests.Support;

namespace UnitTests.TranslationProvidersTests
{
    [TestFixture]
    class YandexTranslateProviderShould
    {
        [Test]
        public void ReturnTranslationForGivenPhrase()
        {
            var urlOpener = new CustomUrlOpener("{\"code\":200, \"text\":[\"translation\"]}");
            var provider = Create.YandexTranslateProvider.WithUrlOpener(urlOpener).Please();
            var translation = provider.Translate("hello world", TranslationDirection.EN_RU);

            Assert.That(translation.Translation, Is.EqualTo("translation"));
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        [TestCase("   ")]
        public void ThrowExceptionIfPhraseIsEmpty(string phrase)
        {
            var urlOpener = new CustomUrlOpener("{\"code\":200, \"text\":[\"translation\"]}");
            var provider = Create.YandexTranslateProvider.WithUrlOpener(urlOpener).Please();
            try
            {
                provider.Translate(phrase, TranslationDirection.EN_RU);
            }
            catch (TranslationException)
            {
                Assert.Pass();
            }
            Assert.Fail("translation provider must throw TranslationException if phrase is empty");
        }

        [Test]
        public void ThrowExceptionOnNetworkError()
        {
            var provider = Create.YandexTranslateProvider.WithUrlOpener(new UrlOpenerWithException()).Please();
            try
            {
                provider.Translate("hello world", TranslationDirection.EN_RU);
            }
            catch (TranslationException)
            {
                Assert.Pass();
            }
            Assert.Fail("translation provider must throw TranslationException on network error");
        }

        [Test]
        public void ThrowExceptionIfServerResponceIsNotValidJSON()
        {
            var provider = Create.YandexTranslateProvider.WithUrlOpener(new CustomUrlOpener("<html></html>")).Please();
            try
            {
                provider.Translate("hello world", TranslationDirection.EN_RU);
            }
            catch (TranslationException)
            {
                Assert.Pass();
            }
            Assert.Fail("translation provider must throw TranslationException on invalid server responce");
        }

        [Test]
        public void ThrowExceptionIfStatusCodeIsNotGiven()
        {
            var provider = Create.YandexTranslateProvider.WithUrlOpener(new CustomUrlOpener("{}")).Please();
            try
            {
                provider.Translate("hello world", TranslationDirection.EN_RU);
            }
            catch (TranslationException)
            {
                Assert.Pass();
            }
            Assert.Fail("translation provider must throw TranslationException if status code is not given");
        }

        [Test]
        public void ThrowExceptionIfStatusCodeIsNot200()
        {
            var provider = Create.YandexTranslateProvider.WithUrlOpener(new CustomUrlOpener("{\"code\": 404}")).Please();
            try
            {
                provider.Translate("hello world", TranslationDirection.EN_RU);
            }
            catch (TranslationException)
            {
                Assert.Pass();
            }
            Assert.Fail("translation provider must throw TranslationException if status code is not 200");
        }

        [Test]
        public void ThrowExceptionIfTranslationResultNotGiven()
        {
            var provider = Create.YandexTranslateProvider.WithUrlOpener(new CustomUrlOpener("{\"code\": 200}")).Please();
            try
            {
                provider.Translate("hello world", TranslationDirection.EN_RU);
            }
            catch (TranslationException)
            {
                Assert.Pass();
            }
            Assert.Fail("translation provider must throw TranslationException if translation result not given");
        }

        [Test]
        public void ThrowExceptionIfTranslationResultIsEmpty()
        {
            var urlOpener = new CustomUrlOpener("{\"code\": 200, \"text\": []}");
            var provider = Create.YandexTranslateProvider.WithUrlOpener(urlOpener).Please();
            try
            {
                provider.Translate("hello world", TranslationDirection.EN_RU);
            }
            catch (TranslationException)
            {
                Assert.Pass();
            }
            Assert.Fail("translation provider must throw TranslationException if translation result is empty");
        }

        [Test]
        public void ThrowExceptionIfSeveralTranslationsWereGiven()
        {
            var urlOpener = new CustomUrlOpener("{\"code\": 200, \"text\": [\"translation1\", \"translation2\"]}");
            var provider = Create.YandexTranslateProvider.WithUrlOpener(urlOpener).Please();
            try
            {
                provider.Translate("hello world", TranslationDirection.EN_RU);
            }
            catch (TranslationException)
            {
                Assert.Pass();
            }
            Assert.Fail("translation provider must throw TranslationException if several translations were given");
        }

        [TestCase("{\"code\": 200, \"text\": [\"\"]}")]
        [TestCase("{\"code\": 200, \"text\": [\"   \"]}")]
        public void ThrowExceptionIfTranslationStringIsEmpty(string serverResponse)
        {
            var provider = Create.YandexTranslateProvider.WithUrlOpener(new CustomUrlOpener(serverResponse)).Please();
            try
            {
                provider.Translate("hello world", TranslationDirection.EN_RU);
            }
            catch (TranslationException)
            {
                Assert.Pass();
            }
            Assert.Fail("translation provider must throw TranslationException if translation string is empty");
        }

        [Test]
        public void ThrowExceptionIfTranslationIsEqualToRequest()
        {
            var response = "{\"code\":200, \"text\":[\"phrase\"]}";
            var provider = Create.YandexTranslateProvider.WithUrlOpener(new CustomUrlOpener(response)).Please();
            try
            {
                provider.Translate("phrase", TranslationDirection.EN_RU);
            }
            catch (TranslationException)
            {
                Assert.Pass();
            }
            Assert.Fail("translation provider must throw TranslationException if translation result is equal to request");
        }
    }
}
