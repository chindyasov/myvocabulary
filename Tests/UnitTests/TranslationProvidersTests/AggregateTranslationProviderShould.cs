﻿using Chindyasov.Vocabulary.TranslationProviders;
using NUnit.Framework;
using UnitTests.Fakes;
using UnitTests.Support;

namespace UnitTests.TranslationProvidersTests
{
    [TestFixture]
    public class AggregateTranslationProviderShould
    {
        [Test]
        public void ThrowExceptionIfItCanNotFetchTranslation()
        {
            var dictionaryProvider = Create.YandexDictionaryProvider.WithUrlOpener(new CustomUrlOpener("{}")).Please();
            var translationProvider = Create.YandexTranslateProvider.WithUrlOpener(new CustomUrlOpener("{}")).Please();

            var aggregateProvider = Create.AggregateTranslationProvider.WithTranslationProvider(translationProvider)
                                                                       .WithDictionaryProvider(dictionaryProvider)
                                                                       .Please();

            try
            {
                aggregateProvider.Translate("cats", TranslationDirection.EN_RU);
            }
            catch (TranslationException)
            {
                Assert.Pass();
            }

            Assert.Fail("translation provider must throw TranslationException if it can not fetch translation");
        }

        [Test]
        public void ReturnDictionaryArticleForSinglewordPhraseOnSuccessTranslationFetching()
        {
            var aggregateProvider = Create.AggregateTranslationProvider.Please();
            var translation = aggregateProvider.Translate("cat", TranslationDirection.EN_RU) as IDictionaryTranslationResult;
            
            Assert.That(translation.Translation, Is.EqualTo("translation 1\ntranslation 2"));
        }

        [Test]
        public void ReturnDictionaryArticleForSinglewordPhraseOnErrorTranslationFetchingUsingTranslationProvider()
        {
            var translationProvider = Create.YandexTranslateProvider.WithUrlOpener(new CustomUrlOpener("{}")).Please();
            var aggregateProvider = Create.AggregateTranslationProvider.WithTranslationProvider(translationProvider).Please();

            var translation = aggregateProvider.Translate("cat", TranslationDirection.EN_RU) as IDictionaryTranslationResult;

            Assert.That(translation.Translation, Is.EqualTo("translation 1\ntranslation 2"));
        }

        [Test]
        public void ReturnTranslationArticleForMultiwordStringOnSuccessTranslationFetching()
        {
            var aggregateProvider = Create.AggregateTranslationProvider.Please();
            var translation = aggregateProvider.Translate("hello world", TranslationDirection.EN_RU);

            Assert.That(translation.Translation, Is.EqualTo("translation"));
        }

        [Test]
        public void ReturnTranslationArticleForSinglewordStringOnSuccessTranslationFetchingUsingTranslationProvider()
        {
            var dictionaryProvider = Create.YandexDictionaryProvider.WithUrlOpener(new CustomUrlOpener("{}")).Please();
            var aggregateProvider = Create.AggregateTranslationProvider.WithDictionaryProvider(dictionaryProvider).Please();

            var translation = aggregateProvider.Translate("hello", TranslationDirection.EN_RU);

            Assert.That(translation.Translation, Is.EqualTo("translation"));
        }
    }
}
