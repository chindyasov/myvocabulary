﻿using NUnit.Framework;
using System;

namespace UnitTests.TranslationProvidersTests
{
    [TestFixture]
    public class YandexTranslationDirectionConverterShould
    {
        [TestCase(TranslationDirection.EN_RU, "en-ru")]
        [TestCase(TranslationDirection.RU_EN, "ru-en")]
        public void ReturnCorrectDirectionStringRepresentation(TranslationDirection direction, string expectedRepresentation)
        {
            Assert.AreEqual(TranslationDirectionConverter.ToString(direction), expectedRepresentation);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void ThrowExceptionIfTranslationDirectionIsInvalid()
        {
            TranslationDirectionConverter.ToString((TranslationDirection)100);
        }
    }
}
