﻿using Chindyasov.Vocabulary.Translator.Views.DataConverters;
using NUnit.Framework;
using System.Windows;

namespace UnitTests
{
    [TestFixture]
    public class BoolToVisibilityConverterShould
    {
        [TestCase(true, Visibility.Visible)]
        [TestCase(false, Visibility.Hidden)]
        public void Convert(bool valueToConvert, Visibility expectedValue)
        {
            var converter = new BoolToVisibilityConverter();
            var convertedValue = converter.Convert(valueToConvert, typeof(Visibility), null, null);
            Assert.That(convertedValue, Is.EqualTo(expectedValue));
        }

        [TestCase(Visibility.Visible, true)]
        [TestCase(Visibility.Hidden, false)]
        [TestCase(Visibility.Collapsed, false)]
        public void ConvertBack(Visibility valueToConvert, bool expectedValue)
        {
            var converter = new BoolToVisibilityConverter();
            var convertedValue = converter.ConvertBack(valueToConvert, typeof(bool), null, null);
            Assert.That(convertedValue, Is.EqualTo(expectedValue));
        }
    }
}
