﻿using Chindyasov.Utils.Net;
using NUnit.Framework;

namespace UnitTests
{
    [TestFixture]
    class UrlOpenerShould
    {
        [TestCase("hello")]
        [TestCase("")]
        [TestCase(null)]
        [ExpectedException(typeof(UrlOperationException))]
        public void ThrowExceptionIfUrlHaveInvalidFormat(string url)
        {
            new UrlOpener().ReadToEnd(url);
        }
    }
}
