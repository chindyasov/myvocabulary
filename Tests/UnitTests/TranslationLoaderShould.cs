﻿using System.Threading;
using Chindyasov.Vocabulary.TranslationProviders;
using NUnit.Framework;
using UnitTests.Fakes;
using UnitTests.Support;

namespace UnitTests
{
    [TestFixture]
    class TranslationLoaderShould
    {
        [Test]
        public void ReturnTranslationOnSuccessTranslationFetching()
        {
            ITranslationResult translationResult = null;
            ManualResetEvent resetEvent = new ManualResetEvent(false);
            var translationProvider = Create.YandexTranslateProvider.Please();

            var translationLoader = Create.TranslationLoader.WithTranslationProvider(translationProvider).Please();

            translationLoader.TranslationLoaded += (s, t) =>
            {
                translationResult = t;
                resetEvent.Set();
            };
            
            translationLoader.StartForPhrase("phrase", TranslationDirection.EN_RU);

            resetEvent.WaitOne(10000);

            Assert.That(translationResult, Is.InstanceOf<ITranslationResult>().Or.InstanceOf<IDictionaryTranslationResult>());
        }
        
        [Test]
        public void RaiseTranslationErrorEventOnTranslationFetchingError()
        {
            bool isTranslationError = false;
            ManualResetEvent resetEvent = new ManualResetEvent(false);
            var translationProvider = Create.YandexTranslateProvider.WithUrlOpener(new UrlOpenerWithException()).Please();

            var translationLoader = Create.TranslationLoader.WithTranslationProvider(translationProvider).Please();

            translationLoader.TranslationError += (s, e) =>
            {
                isTranslationError = true;
                resetEvent.Set();
            };
            
            translationLoader.StartForPhrase("phrase", TranslationDirection.EN_RU);

            resetEvent.WaitOne(10000);

            Assert.IsTrue(isTranslationError);
        }
    }
}
