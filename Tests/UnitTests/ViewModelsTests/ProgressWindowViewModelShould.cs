﻿using NUnit.Framework;
using UnitTests.Support;
using UnitTests.Fakes;

namespace UnitTests.ViewModelsTests
{
    [TestFixture]
    class ProgressWindowViewModelShould
    {
        private FakeDimensions _dimensions;
        private FakeTranslationLoader _translationLoader;
        private ViewModelsRepository _repository;

        [SetUp]
        public void Initialize()
        {
            _dimensions = new FakeDimensions() { ScreenWidth = 800, ScreenHeight = 600, MainViewHeight = 50, MainViewWidth = 500 };
            _translationLoader = new FakeTranslationLoader();
            _repository = Create.ViewModelsRepository.WithDimensions(_dimensions).WithTranslationLoader(_translationLoader).Please();

            _repository.MainWindowViewModel.ShowView();
        }

        [Test]
        public void BePlacedBelowMainWindow()
        {
            Assert.That(_repository.ProgressWindowViewModel.Top, Is.EqualTo(_repository.MainWindowViewModel.Top + _dimensions.MainViewHeight));
        }

        [Test]
        public void BePlacedAtCenterScheenHorisontal()
        {
            Assert.That(_repository.ProgressWindowViewModel.Left, Is.EqualTo(150));
        }

        [Test]
        public void NotBeDisplayedBeforeSearchAction()
        {
            Assert.That(_repository.ProgressWindowViewModel.IsVisible, Is.False);
        }

        [Test]
        public void BeDisplayedOnSearchAction()
        {
            _repository.PhraseInputViewModel.Text = "text to translate";
            _repository.PhraseInputViewModel.EnterButtonPressedAction.Invoke();
            Assert.That(_repository.ProgressWindowViewModel.IsVisible, Is.True);
        }

        [Test]
        public void BeClosedOnEscPressed()
        {
            _repository.ProgressWindowViewModel.EscapeKeyPressedAction.Invoke();
            Assert.That(_repository.ProgressWindowViewModel.IsVisible, Is.False);
        }

        [Test]
        public void BeClosedAfterTranslationResultViewWasDisplayed()
        {
            _repository.PhraseInputViewModel.Text = "text to translate";
            _repository.PhraseInputViewModel.EnterButtonPressedAction.Invoke();

            _translationLoader.RaiseTranslationReadyEvent();

            Assert.That(_repository.ProgressWindowViewModel.IsVisible, Is.False);
        }

        [Test]
        public void BeClosedIfMainWindowClosed()
        {
            _repository.PhraseInputViewModel.Text = "text to translate";
            _repository.PhraseInputViewModel.EnterButtonPressedAction.Invoke();
            _repository.MainWindowViewModel.HideView();
            Assert.That(_repository.ProgressWindowViewModel.IsVisible, Is.False);
        }

        [Test]
        public void BeClosedIfPhraseWasChanged()
        {
            _repository.PhraseInputViewModel.Text = "text to translate";
            _repository.PhraseInputViewModel.EnterButtonPressedAction.Invoke();
            _repository.PhraseInputViewModel.Text = "changed text";

            Assert.That(_repository.ProgressWindowViewModel.IsVisible, Is.False);
        }
    }
}
