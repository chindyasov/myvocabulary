﻿using NUnit.Framework;
using UnitTests.Support;
using UnitTests.Fakes;
using Chindyasov.Vocabulary.Translator;
using Chindyasov.Utils;

namespace UnitTests.ViewModelsTests
{
    [TestFixture]
    class MainWindowViewModelShould
    {
        private IDimensions _dimensions;
        private FakeTranslationLoader _translationLoader;
        private ViewModelsRepository _repository;
        private FakeHotKeyHandler _hotKeyHandler;

        [SetUp]
        public void Initialize()
        {
            _translationLoader = new FakeTranslationLoader();
            _dimensions = new FakeDimensions() { ScreenHeight = 600, ScreenWidth = 800, MainViewHeight = 70, MainViewWidth = 500 };
            _hotKeyHandler = new FakeHotKeyHandler();
            _repository = Create.ViewModelsRepository.WithTranslationLoader(_translationLoader)
                                                     .WithDimensions(_dimensions)
                                                     .WithHotKeyHandler(_hotKeyHandler)
                                                     .Please();
        }

        [Test]
        public void DoNotDisplayedAtUserScreenAfterApplicationStart()
        {
            _repository.MainWindowViewModel.ShowWithStartupState();

            Assert.That(_repository.MainWindowViewModel.Top, Is.EqualTo(-_dimensions.MainViewHeight));
            Assert.That(_repository.MainWindowViewModel.IsVisible, Is.True);
        }

        [Test]
        public void ShowMainWindowView()
        {
            _repository.MainWindowViewModel.ShowView();
            Assert.That(_repository.MainWindowViewModel.IsVisible, Is.True);
        }

        [Test]
        public void ShowMainWindowOnTopCenterPosition()
        {
            _repository.MainWindowViewModel.ShowView();

            Assert.That(_repository.MainWindowViewModel.Left, Is.EqualTo(150));
            Assert.That(_repository.MainWindowViewModel.Top, Is.EqualTo(0));
        }

        [Test]
        public void DisplayTranslationWindowWhenInputContainsText()
        {
            _repository.MainWindowViewModel.ShowView();

            _repository.PhraseInputViewModel.Text = "fake phrase";
            _repository.PhraseInputViewModel.EnterButtonPressedAction.Invoke();
            _translationLoader.RaiseTranslationReadyEvent();
            Assert.That(_repository.TranslationViewModel.IsVisible, Is.True);
        }

        [Test]
        public void DoNotDisplayTranslationWindowWhenInputIsEmpty()
        {
            _repository.MainWindowViewModel.ShowView();

            _repository.PhraseInputViewModel.Text = string.Empty;
            _repository.PhraseInputViewModel.EnterButtonPressedAction.Invoke();

            Assert.That(_repository.TranslationViewModel.IsVisible, Is.False);
        }

        [Test]
        public void BeVisibleOnHotKey()
        {
            _hotKeyHandler.OnHotKeyDetected();
            Assert.That(_repository.MainWindowViewModel.IsVisible, Is.True);
        }

        [Test]
        public void BeClosedOnHotKey()
        {
            _repository.MainWindowViewModel.ShowView();
            _hotKeyHandler.OnHotKeyDetected();
            Assert.That(_repository.MainWindowViewModel.IsVisible, Is.False);
        }

        [Test]
        public void BeClosedOnEscPressed()
        {
            _repository.MainWindowViewModel.ShowView();
            _repository.MainWindowViewModel.EscapeKeyPressedAction.Invoke();
            Assert.That(_repository.MainWindowViewModel.IsVisible, Is.False);
        }

        [Test]
        public void BeClosedOnCloseButtonClick()
        {
            _repository.MainWindowViewModel.ShowView();
            _repository.CloseButtonViewModel.LeftButtonDown();
            _repository.CloseButtonViewModel.LeftButtonUp();
            Assert.That(_repository.MainWindowViewModel.IsVisible, Is.False);
        }

        [Test]
        public void BeClosedOnDownloadLinkClickInUpdateWindow()
        {
            var updateManager = Create.UpdateManager.WithCurrentVersion(new AppVersion("0.1.0")).Please();
            var dimensions = new FakeDimensions() { ScreenHeight = 600, ScreenWidth = 800, MainViewHeight = 70, MainViewWidth = 500 };
            _repository = Create.ViewModelsRepository.WithDimensions(dimensions).WithUpdateManager(updateManager).Please();

            _repository.MainWindowViewModel.ShowView();

            _repository.UpdateWindowViewModel.DownloadAction.Invoke();

            Assert.That(_repository.MainWindowViewModel.IsVisible, Is.False);
        }

        [Test]
        public void DisplayTranslationAfterEnterButtonPressed()
        {
            var translationResult = new FakeTranslationResult("translation for fake phrase");
            _translationLoader = new FakeTranslationLoader(translationResult);
            _dimensions = new FakeDimensions() { ScreenHeight = 600, ScreenWidth = 800, MainViewHeight = 70, MainViewWidth = 500 };
            _repository = Create.ViewModelsRepository.WithTranslationLoader(_translationLoader).WithDimensions(_dimensions).Please();

            _repository.MainWindowViewModel.ShowView();

            _repository.PhraseInputViewModel.Text = "fake phrase";
            _repository.PhraseInputViewModel.EnterButtonPressedAction.Invoke();
            _translationLoader.RaiseTranslationReadyEvent();

            Assert.That(_repository.TranslationViewModel.TranslationResult, Is.SameAs(translationResult));
        }

        [Test]
        public void HaveEnToRuTranslationDirectionAtStartup()
        {
            Assert.AreEqual(TranslationDirection.EN_RU, _repository.MainWindowViewModel.CurrentTranslationDirection);
        }

        [Test]
        public void ChangeTranslationDirectionAfterDirectionIndicatorWasClicked()
        {
            _repository.MainWindowViewModel.TranslationDirectionClickAction.Invoke();
            Assert.AreEqual(TranslationDirection.RU_EN, _repository.MainWindowViewModel.CurrentTranslationDirection);
        }

        [Test]
        public void LoadTranslationWithCurrentTranslationDirection()
        {
            var translationLoader = new FakeTranslationLoader();
            _repository = Create.ViewModelsRepository.WithTranslationLoader(translationLoader).Please();
            
            _repository.MainWindowViewModel.ShowView();
            
            _repository.PhraseInputViewModel.Text = "fake phrase";

            _repository.MainWindowViewModel.TranslationDirectionClickAction.Invoke();
            _repository.PhraseInputViewModel.EnterButtonPressedAction.Invoke();

            Assert.AreEqual(TranslationDirection.RU_EN, translationLoader.LastDirection);
        }

        [TestCase("hello", TranslationDirection.EN_RU)]
        [TestCase("  hello World!!! ", TranslationDirection.EN_RU)]
        [TestCase("Здравствуй, мир!", TranslationDirection.RU_EN)]
        public void AutodetectPhraseLanguage(string phrase, TranslationDirection expectedDirection)
        {
            _repository.PhraseInputViewModel.Text = phrase;
            Assert.AreEqual(expectedDirection, _repository.MainWindowViewModel.CurrentTranslationDirection);
        }

        [Test]
        public void SaveTranslationDirectionAfterLoseFocusAndEmptyPhraseInputField()
        {
            _repository.PhraseInputViewModel.Text = "привет";
            _repository.PhraseInputViewModel.Text = "";

            _repository.PhraseInputViewModel.LostFocusAction.Invoke();

            Assert.AreEqual(TranslationDirection.RU_EN, _repository.MainWindowViewModel.CurrentTranslationDirection);
        }
    }
}
