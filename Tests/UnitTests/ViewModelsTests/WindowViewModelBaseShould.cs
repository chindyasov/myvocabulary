﻿using NUnit.Framework;
using UnitTests.Fakes;

namespace UnitTests.ViewModelsTests
{
    [TestFixture]
    public class WindowViewModelBaseShould
    {
        [Test]
        public void HandleScreenDimensionChange()
        {
            var dimensions = new FakeDimensions() { ScreenHeight = 600, ScreenWidth = 800, MainViewHeight = 70, MainViewWidth = 500 };
            var viewModel = new WindowViewModelBaseFake(dimensions);  // this fake view model contains basic logic of WindowViewModelBase

            viewModel.IsVisible = true;

            dimensions.ScreenWidth = 700;

            viewModel.IsVisible = false;
            viewModel.IsVisible = true;

            Assert.AreEqual(100, viewModel.Left);
        }
    }
}
