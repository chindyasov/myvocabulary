﻿using NUnit.Framework;
using UnitTests.Support;

namespace UnitTests.ViewModelsTests
{
    [TestFixture]
    class PhraseInputViewModelShould
    {
        ViewModelsRepository _repository;

        [SetUp]
        public void Initialize()
        {
            _repository = Create.ViewModelsRepository.Please();
        }

        [Test]
        public void ContainsSuggestionAfterShow()
        {
            Assert.That(_repository.PhraseInputViewModel.Text, Is.EqualTo("enter a text to translate"));
            Assert.That(_repository.PhraseInputViewModel.IsSuggestionMode, Is.True);
        }

        [Test]
        public void DoNotContainsSuggestionIfUserInputedChar()
        {
            _repository.PhraseInputViewModel.Text = "a" + _repository.PhraseInputViewModel.Text;
            Assert.That(_repository.PhraseInputViewModel.Text, Is.EqualTo("a"));
            Assert.That(_repository.PhraseInputViewModel.IsSuggestionMode, Is.False);
        }

        [Test]
        public void DoNotContainsSuggestionIfUserClickedToInputBox()
        {
            _repository.PhraseInputViewModel.MouseButtonDownAction.Invoke();
            Assert.That(_repository.PhraseInputViewModel.Text, Is.EqualTo(""));
            Assert.That(_repository.PhraseInputViewModel.IsSuggestionMode, Is.False);
        }

        [Test]
        public void ContainsSuggestionAfterLostFocusAndEmptyInputBox()
        {
            _repository.PhraseInputViewModel.MouseButtonDownAction.Invoke();
            _repository.PhraseInputViewModel.LostFocusAction.Invoke();
            Assert.That(_repository.PhraseInputViewModel.Text, Is.EqualTo("enter a text to translate"));
            Assert.That(_repository.PhraseInputViewModel.IsSuggestionMode, Is.True);
        }

        [Test]
        public void ClearPreviousInputAfterHideAction()
        {
            _repository.MainWindowViewModel.ShowWithStartupState();
            _repository.MainWindowViewModel.ShowView();

            _repository.PhraseInputViewModel.Text = "custom user input";

            _repository.MainWindowViewModel.HideView();
            _repository.MainWindowViewModel.ShowView();

            Assert.That(_repository.PhraseInputViewModel.Text, Is.EqualTo("enter a text to translate"));
            Assert.That(_repository.PhraseInputViewModel.IsSuggestionMode, Is.True);
        }
    }
}
