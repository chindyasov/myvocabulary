﻿using NUnit.Framework;
using UnitTests.Support;
using UnitTests.Fakes;

namespace UnitTests.ViewModelsTests
{
    [TestFixture]
    class TranslationViewModelShould
    {
        [Test]
        public void BePlacedBelowMainView()
        {
            var dimensions = new FakeDimensions() { ScreenHeight = 600, ScreenWidth = 800, MainViewHeight = 70, MainViewWidth = 500 };
            var repository = Create.ViewModelsRepository.WithDimensions(dimensions).Please();

            repository.MainWindowViewModel.ShowView();
            repository.TranslationViewModel.ShowTranslation(new FakeTranslationResult());

            Assert.That(repository.TranslationViewModel.Top, Is.EqualTo(repository.MainWindowViewModel.Top + dimensions.MainViewHeight));
        }

        [Test]
        public void BeClosedIfMainViewModelWasClosed()
        {
            var repository = Create.ViewModelsRepository.Please();

            repository.MainWindowViewModel.ShowView();
            repository.TranslationViewModel.ShowTranslation(new FakeTranslationResult());
            repository.MainWindowViewModel.HideView();

            Assert.That(repository.TranslationViewModel.IsVisible, Is.False);
        }

        [Test]
        public void BeClosedOnEscPressed()
        {
            var repository = Create.ViewModelsRepository.Please();

            repository.MainWindowViewModel.ShowView();
            repository.TranslationViewModel.ShowTranslation(new FakeTranslationResult());
            repository.TranslationViewModel.EscapeKeyPressedAction.Invoke();

            Assert.That(repository.TranslationViewModel.IsVisible, Is.False);
        }

        [Test]
        public void BeShownNormallyOnNextSearchAfterTranslationWasCanceled()
        {
            var translationLoader = new FakeTranslationLoader();
            var repository = Create.ViewModelsRepository.WithTranslationLoader(translationLoader).Please();

            repository.MainWindowViewModel.ShowView();

            repository.PhraseInputViewModel.Text = "text to translate";
            repository.PhraseInputViewModel.EnterButtonPressedAction.Invoke();

            repository.PhraseInputViewModel.Text = "changed text";
            repository.PhraseInputViewModel.EnterButtonPressedAction.Invoke();

            translationLoader.RaiseTranslationReadyEvent();

            Assert.That(repository.TranslationViewModel.IsVisible, Is.True);
        }

        [Test]
        public void BeClosedOnEnterButtonWasClickedInPhraseInputField()
        {
            var repository = Create.ViewModelsRepository.Please();

            repository.MainWindowViewModel.ShowView();
            repository.PhraseInputViewModel.Text = "phrase to translate";
            repository.TranslationViewModel.ShowTranslation(new FakeTranslationResult());

            repository.PhraseInputViewModel.EnterButtonPressedAction.Invoke();

            Assert.That(repository.TranslationViewModel.IsVisible, Is.False);
        }

        [Test]
        public void BeClosedIfPhraseWasChanged()
        {
            var repository = Create.ViewModelsRepository.Please();

            repository.MainWindowViewModel.ShowView();
            repository.TranslationViewModel.ShowTranslation(new FakeTranslationResult());
            repository.PhraseInputViewModel.Text = "updated text";

            Assert.That(repository.TranslationViewModel.IsVisible, Is.False);
        }

        [Test]
        public void DisplayVendorInfoFromTranslationResult()
        {
            var translationResult = new FakeTranslationResult("translation for fake phrase", "Yandex.Translate", "http://translate.ya.ru");
            var translationLoader = new FakeTranslationLoader(translationResult);
            var repository = Create.ViewModelsRepository.WithTranslationLoader(translationLoader).Please();

            repository.MainWindowViewModel.ShowView();

            repository.PhraseInputViewModel.Text = "fake phrase";
            repository.PhraseInputViewModel.EnterButtonPressedAction.Invoke();
            translationLoader.RaiseTranslationReadyEvent();

            Assert.That(repository.TranslationViewModel.VendorName, Is.EqualTo("Yandex.Translate"));
            Assert.That(repository.TranslationViewModel.VendorUrl, Is.EqualTo("http://translate.ya.ru"));
        }

        [Test]
        public void OpenBrowserWhenUserClickedOnVendorName()
        {
            var translationResult = new FakeTranslationResult("translation for fake phrase", "Yandex.Translate", "http://translate.ya.ru");
            var browserOpener = new FakeBrowserOpener();
            var translationLoader = new FakeTranslationLoader(translationResult);
            var repository = Create.ViewModelsRepository.WithTranslationLoader(translationLoader).WithBrowserOpener(browserOpener).Please();

            repository.MainWindowViewModel.ShowView();

            repository.PhraseInputViewModel.Text = "fake phrase";
            repository.PhraseInputViewModel.EnterButtonPressedAction.Invoke();
            translationLoader.RaiseTranslationReadyEvent();

            repository.TranslationViewModel.VendorLinkClickedAction.Invoke();

            Assert.IsTrue(browserOpener.WasOpened);
            Assert.AreEqual("http://translate.ya.ru", browserOpener.LoadedUrl);
        }
    }
}
