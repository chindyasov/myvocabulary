﻿using Chindyasov.Utils;
using Chindyasov.Vocabulary.Translator;
using NUnit.Framework;
using UnitTests.Fakes;
using UnitTests.Support;

namespace UnitTests.ViewModelsTests
{
    [TestFixture]
    public class UpdateWindowViewModelShould
    {
        private IDimensions _dimensions;
        private ViewModelsRepository _repository;

        [SetUp]
        public void Initialize()
        {
            _dimensions = new FakeDimensions() { ScreenHeight = 600, ScreenWidth = 800, MainViewHeight = 70, MainViewWidth = 500 };
            _repository = Create.ViewModelsRepository.WithDimensions(_dimensions).Please();
        }

        [Test]
        public void BeInvisibleAtApplicationStartup()
        {
            _repository.MainWindowViewModel.ShowWithStartupState();

            Assert.That(_repository.UpdateWindowViewModel.IsVisible, Is.False);
        }

        [Test]
        public void BeVisibleWhenMainWindowIsVisibleAndUpdateIsAvailable()
        {
            var updateManager = Create.UpdateManager.WithCurrentVersion(new AppVersion("0.1.0")).Please();
            _repository = Create.ViewModelsRepository.WithUpdateManager(updateManager).Please();
            _repository.MainWindowViewModel.ShowView();

            Assert.That(_repository.UpdateWindowViewModel.IsVisible, Is.True);
        }

        [Test]
        public void BePlacedBelowMainViewAndVerticalCentered()
        {
            var updateManager = Create.UpdateManager.WithCurrentVersion(new AppVersion("0.1.0")).Please();
            var dimensions = new FakeDimensions() { ScreenHeight = 600, ScreenWidth = 800, MainViewHeight = 70, MainViewWidth = 500 };
            _repository = Create.ViewModelsRepository.WithDimensions(dimensions).WithUpdateManager(updateManager).Please();

            _repository.MainWindowViewModel.ShowView();

            Assert.That(_repository.UpdateWindowViewModel.Top, Is.EqualTo(_repository.MainWindowViewModel.Top + dimensions.MainViewHeight));
            Assert.That(_repository.UpdateWindowViewModel.Left, Is.EqualTo((dimensions.ScreenWidth - dimensions.MainViewWidth) / 2));
        }

        [Test]
        public void NotBeVisibleWhenMainWindowIsVisibleAndUpdateIsNotAvailable()
        {
            var updateManager = Create.UpdateManager.WithCurrentVersion(new AppVersion("1.0.0")).Please();
            _repository = Create.ViewModelsRepository.WithUpdateManager(updateManager).Please();
            _repository.MainWindowViewModel.ShowView();

            Assert.That(_repository.UpdateWindowViewModel.IsVisible, Is.False);
        }

        [Test]
        public void BeClosedWhenMainViewWasClosed()
        {
            var updateManager = Create.UpdateManager.WithCurrentVersion(new AppVersion("0.1.0")).Please();
            _repository = Create.ViewModelsRepository.WithUpdateManager(updateManager).Please();
            _repository.MainWindowViewModel.ShowView();

            _repository.MainWindowViewModel.HideView();

            Assert.That(_repository.UpdateWindowViewModel.IsVisible, Is.False);
        }

        [Test]
        public void BeClosedOnEscPressed()
        {
            var updateManager = Create.UpdateManager.WithCurrentVersion(new AppVersion("0.1.0")).Please();
            var dimensions = new FakeDimensions() { ScreenHeight = 600, ScreenWidth = 800, MainViewHeight = 70, MainViewWidth = 500 };
            _repository = Create.ViewModelsRepository.WithDimensions(dimensions).WithUpdateManager(updateManager).Please();

            _repository.MainWindowViewModel.ShowView();

            _repository.UpdateWindowViewModel.EscapeKeyPressedAction.Invoke();

            Assert.That(_repository.UpdateWindowViewModel.IsVisible, Is.False);
        }

        [Test]
        public void BeClosedOnPhraseSearchStart()
        {
            var updateManager = Create.UpdateManager.WithCurrentVersion(new AppVersion("0.1.0")).Please();
            var dimensions = new FakeDimensions() { ScreenHeight = 600, ScreenWidth = 800, MainViewHeight = 70, MainViewWidth = 500 };
            _repository = Create.ViewModelsRepository.WithDimensions(dimensions).WithUpdateManager(updateManager).Please();

            _repository.MainWindowViewModel.ShowView();

            _repository.PhraseInputViewModel.Text = "phrase";
            _repository.PhraseInputViewModel.EnterButtonPressedAction.Invoke();

            Assert.That(_repository.UpdateWindowViewModel.IsVisible, Is.False);
        }

        [Test]
        public void BeClosedOnDownloadUpdateLinkClick()
        {
            var updateManager = Create.UpdateManager.WithCurrentVersion(new AppVersion("0.1.0")).Please();
            var dimensions = new FakeDimensions() { ScreenHeight = 600, ScreenWidth = 800, MainViewHeight = 70, MainViewWidth = 500 };
            _repository = Create.ViewModelsRepository.WithDimensions(dimensions).WithUpdateManager(updateManager).Please();

            _repository.MainWindowViewModel.ShowView();

            _repository.UpdateWindowViewModel.DownloadAction.Invoke();

            Assert.That(_repository.UpdateWindowViewModel.IsVisible, Is.False);
        }

        [Test]
        public void OpenBrowserWhenUserClicksOnDownloadLink()
        {
            var browserOpener = new FakeBrowserOpener();
            var updateManager = Create.UpdateManager.WithBrowserOpener(browserOpener).WithCurrentVersion(new AppVersion("0.1.0")).Please();
            var dimensions = new FakeDimensions() { ScreenHeight = 600, ScreenWidth = 800, MainViewHeight = 70, MainViewWidth = 500 };
            _repository = Create.ViewModelsRepository.WithDimensions(dimensions).WithUpdateManager(updateManager).Please();

            _repository.MainWindowViewModel.ShowView();

            _repository.UpdateWindowViewModel.DownloadAction.Invoke();

            Assert.That(browserOpener.LoadedUrl, Is.EqualTo("http://update.com"));
        }
    }
}
