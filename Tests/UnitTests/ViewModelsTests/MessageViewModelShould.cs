﻿using NUnit.Framework;
using UnitTests.Fakes;
using UnitTests.Support;

namespace UnitTests.ViewModelsTests
{
    [TestFixture]
    class MessageViewModelShould
    {
        [Test]
        public void BePlacedBelowMainView()
        {
            var translationResult = new FakeTranslationResult("translation for fake phrase", "Yandex.Translate", "http://translate.ya.ru");
            var translationLoader = new FakeTranslationLoader(translationResult);
            var dimensions = new FakeDimensions() { ScreenHeight = 600, ScreenWidth = 800, MainViewHeight = 70, MainViewWidth = 500 };
            var repository = Create.ViewModelsRepository.WithTranslationLoader(translationLoader).WithDimensions(dimensions).Please();

            repository.MainWindowViewModel.ShowView();

            repository.PhraseInputViewModel.Text = "fake phrase";
            repository.PhraseInputViewModel.EnterButtonPressedAction.Invoke();

            translationLoader.RaiseTranslationErrorEvent();

            Assert.That(repository.MessageViewModel.Top, Is.EqualTo(repository.MainWindowViewModel.Top + dimensions.MainViewHeight));
        }

        [Test]
        public void BePlacedAtCenterScheenHorisontal()
        {
            var translationResult = new FakeTranslationResult("translation for fake phrase", "Yandex.Translate", "http://translate.ya.ru");
            var translationLoader = new FakeTranslationLoader(translationResult);
            var dimensions = new FakeDimensions() { ScreenHeight = 600, ScreenWidth = 800, MainViewHeight = 70, MainViewWidth = 500 };
            var repository = Create.ViewModelsRepository.WithTranslationLoader(translationLoader).WithDimensions(dimensions).Please();

            repository.MainWindowViewModel.ShowView();

            repository.PhraseInputViewModel.Text = "fake phrase";
            repository.PhraseInputViewModel.EnterButtonPressedAction.Invoke();

            translationLoader.RaiseTranslationErrorEvent();

            Assert.That(repository.MessageViewModel.Left, Is.EqualTo(150));
        }

        [Test]
        public void BeVisibleOnTranslationError()
        {
            var translationResult = new FakeTranslationResult("translation for fake phrase", "Yandex.Translate", "http://translate.ya.ru");
            var translationLoader = new FakeTranslationLoader(translationResult);
            var repository = Create.ViewModelsRepository.WithTranslationLoader(translationLoader).Please();

            repository.MainWindowViewModel.ShowView();

            repository.PhraseInputViewModel.Text = "fake phrase";
            repository.PhraseInputViewModel.EnterButtonPressedAction.Invoke();

            translationLoader.RaiseTranslationErrorEvent();

            Assert.That(repository.MessageViewModel.IsVisible, Is.True);
        }

        [Test]
        public void BeClosedWhenUserChangedPhrase()
        {
            var translationResult = new FakeTranslationResult("translation for fake phrase", "Yandex.Translate", "http://translate.ya.ru");
            var translationLoader = new FakeTranslationLoader(translationResult);
            var repository = Create.ViewModelsRepository.WithTranslationLoader(translationLoader).Please();

            repository.MainWindowViewModel.ShowView();

            repository.PhraseInputViewModel.Text = "fake phrase";
            repository.PhraseInputViewModel.EnterButtonPressedAction.Invoke();

            translationLoader.RaiseTranslationErrorEvent();

            repository.PhraseInputViewModel.Text = "new phrase";

            Assert.That(repository.MessageViewModel.IsVisible, Is.False);
        }

        [Test]
        public void BeClosedWhenUserPressedEscButtonOnMainView()
        {
            var translationResult = new FakeTranslationResult("translation for fake phrase", "Yandex.Translate", "http://translate.ya.ru");
            var translationLoader = new FakeTranslationLoader(translationResult);
            var repository = Create.ViewModelsRepository.WithTranslationLoader(translationLoader).Please();

            repository.MainWindowViewModel.ShowView();

            repository.PhraseInputViewModel.Text = "fake phrase";
            repository.PhraseInputViewModel.EnterButtonPressedAction.Invoke();

            translationLoader.RaiseTranslationErrorEvent();

            repository.MainWindowViewModel.EscapeKeyPressedAction.Invoke();

            Assert.That(repository.MessageViewModel.IsVisible, Is.False);
        }

        [Test]
        public void BeClosedWhenUserPressedEscButtonOnMessageView()
        {
            var translationResult = new FakeTranslationResult("translation for fake phrase", "Yandex.Translate", "http://translate.ya.ru");
            var translationLoader = new FakeTranslationLoader(translationResult);
            var repository = Create.ViewModelsRepository.WithTranslationLoader(translationLoader).Please();

            repository.MainWindowViewModel.ShowView();

            repository.PhraseInputViewModel.Text = "fake phrase";
            repository.PhraseInputViewModel.EnterButtonPressedAction.Invoke();

            translationLoader.RaiseTranslationErrorEvent();

            repository.MessageViewModel.EscapeKeyPressedAction.Invoke();

            Assert.That(repository.MessageViewModel.IsVisible, Is.False);
        }

        public void BeClosedWhenUserClickedOnCloseButton()
        {
            var translationResult = new FakeTranslationResult("translation for fake phrase", "Yandex.Translate", "http://translate.ya.ru");
            var translationLoader = new FakeTranslationLoader(translationResult);
            var repository = Create.ViewModelsRepository.WithTranslationLoader(translationLoader).Please();

            repository.MainWindowViewModel.ShowView();

            repository.PhraseInputViewModel.Text = "fake phrase";
            repository.PhraseInputViewModel.EnterButtonPressedAction.Invoke();

            translationLoader.RaiseTranslationErrorEvent();

            repository.CloseButtonViewModel.LeftButtonDown();
            repository.CloseButtonViewModel.LeftButtonUp();

            Assert.That(repository.MessageViewModel.IsVisible, Is.False);
        }

        [Test]
        public void BeInvisibleOnSuccesTranslationFetching()
        {
            var translationResult = new FakeTranslationResult("translation for fake phrase", "Yandex.Translate", "http://translate.ya.ru");
            var translationLoader = new FakeTranslationLoader(translationResult);
            var repository = Create.ViewModelsRepository.WithTranslationLoader(translationLoader).Please();

            repository.MainWindowViewModel.ShowView();

            repository.PhraseInputViewModel.Text = "fake phrase";
            repository.PhraseInputViewModel.EnterButtonPressedAction.Invoke();

            translationLoader.RaiseTranslationReadyEvent();

            Assert.That(repository.MessageViewModel.IsVisible, Is.False);
        }
    }
}
