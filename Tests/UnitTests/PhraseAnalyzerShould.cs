﻿using Chindyasov.Utils;
using NUnit.Framework;

namespace UnitTests
{
    [TestFixture]
    class PhraseAnalyzerShould
    {
        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        [TestCase("   ")]
        public void DetectEmptyPhraseIfPhraseIsNullOrEmpty(string phrase)
        {
            Assert.That(Phrase.DetectPhraseType(phrase), Is.EqualTo(PhraseType.Empty));
        }

        [TestCase("word")]
        [TestCase("self-cleaning")]
        [TestCase("  word  ")]
        public void DetectSingleWordPhrase(string phrase)
        {
            Assert.That(Phrase.DetectPhraseType(phrase), Is.EqualTo(PhraseType.SingleWord));
        }

        [TestCase("multiword phrase")]
        [TestCase("  multiword phrase  ")]
        [TestCase("multiword phrase with more than 2 words")]
        public void DetectMultiWordPhrase(string phrase)
        {
            Assert.That(Phrase.DetectPhraseType(phrase), Is.EqualTo(PhraseType.MultiWord));
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        [TestCase("   ")]
        public void NormalizeEmptyString(string phrase)
        {
            Assert.That(Phrase.Normalize(phrase), Is.EqualTo(""));
        }

        [TestCase("phrase")]
        [TestCase(" phrase ")]
        [TestCase("    phrase  ")]
        public void NormalizeSingleWordPhrase(string phrase)
        {
            Assert.That(Phrase.Normalize(phrase), Is.EqualTo("phrase"));
        }

        [TestCase("    it is multiword phrase  ")]
        [TestCase("it     is     multiword phrase")]
        [TestCase("   it     is     multiword phrase   ")]
        public void NormalizeMultiWordPhrase(string phrase)
        {
            Assert.That(Phrase.Normalize(phrase), Is.EqualTo("it is multiword phrase"));
        }

        [Test]
        public void RemoveSpacesBetweenPunctuationCharacters()
        {
            var phrase = "test !! ! test test , test? ?? test . . .";
            Assert.That(Phrase.Normalize(phrase), Is.EqualTo("test!!! test test, test??? test..."));
        }

        [TestCase(null, PhraseLanguage.Unknown)]
        [TestCase("", PhraseLanguage.Unknown)]
        [TestCase("   ", PhraseLanguage.Unknown)]
        [TestCase(" Hello  ", PhraseLanguage.En)]
        [TestCase(" I  ", PhraseLanguage.En)]
        [TestCase(" HELLO ", PhraseLanguage.En)]
        [TestCase("  ПРИВЕТ  ", PhraseLanguage.Ru)]
        [TestCase(" привет  ", PhraseLanguage.Ru)]
        [TestCase(" hi! привет  ", PhraseLanguage.Ru)]
        [TestCase(" hello wold! привет  ", PhraseLanguage.En)]
        [TestCase("ыыыfff", PhraseLanguage.En)]
        [TestCase("ё", PhraseLanguage.Ru)]
        public void DetectPhraseLanguage(string phrase, PhraseLanguage expectedLanguage)
        {
            Assert.That(Phrase.DetectLanguage(phrase), Is.EqualTo(expectedLanguage));
        }
    }
}
