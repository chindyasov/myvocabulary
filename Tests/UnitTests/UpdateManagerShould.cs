﻿using Chindyasov.Utils;
using NUnit.Framework;
using System;
using UnitTests.Fakes;
using UnitTests.Support;

namespace UnitTests
{
    [TestFixture]
    public class UpdateManagerShould
    {
        [Test]
        public void ReturnTrueIfUpdateIsEnabledAndIsAvailable()
        {
            var updateManager = Create.UpdateManager.WithCurrentVersion(new AppVersion("0.1.0")).Please();
            Assert.IsTrue(updateManager.IsNeedToShowUpdateDialog);
        }

        [Test]
        public void ReturnFalseIfUpdateIsNotEnabled()
        {
            var urlOpener = new CustomUrlOpener("{\"Enabled\": false, \"StableVersion\": \"1.0.0\", \"UpdatePageUrl\": \"http://update.com\"}");
            var currentVersion = new AppVersion("0.1.0");
            var updateManager = Create.UpdateManager.WithUrlOpener(urlOpener).WithCurrentVersion(currentVersion).Please();
            Assert.IsFalse(updateManager.IsNeedToShowUpdateDialog);
        }

        [TestCase("1.0.0")]
        [TestCase("2.1.1")]
        public void ReturnFalseIfUpdateIsEnabledButIsNotAvailable(string currentVersion)
        {
            var urlOpener = new CustomUrlOpener("{\"Enabled\": true, \"StableVersion\": \"1.0.0\", \"UpdatePageUrl\": \"http://update.com\"}");
            var version = new AppVersion(currentVersion);
            var updateManager = Create.UpdateManager.WithUrlOpener(urlOpener).WithCurrentVersion(version).Please();
            Assert.IsFalse(updateManager.IsNeedToShowUpdateDialog);
        }

        [Test]
        public void ReturnFalseOnNetworkError()
        {
            var urlOpener = new UrlOpenerWithException();
            var updateManager = Create.UpdateManager.WithUrlOpener(urlOpener).Please();
            Assert.IsFalse(updateManager.IsNeedToShowUpdateDialog);
        }

        [Test]
        public void ReturnFalseIfJsonIsIncorrect()
        {
            var urlOpener = new CustomUrlOpener("{}");
            var updateManager = Create.UpdateManager.WithUrlOpener(urlOpener).Please();
            Assert.IsFalse(updateManager.IsNeedToShowUpdateDialog);
        }

        [Test]
        public void ShouldOpenBrowserAndNavigateUpdatePage()
        {
            var browserOpener = new FakeBrowserOpener();
            var updateManager = Create.UpdateManager.WithBrowserOpener(browserOpener).WithCurrentVersion(new AppVersion("0.1.0")).Please();

            updateManager.OpenUpdatePage();

            Assert.IsTrue(browserOpener.WasOpened);
            Assert.That(browserOpener.LoadedUrl, Is.EqualTo("http://update.com"));
        }

        [Test]
        public void UpdateSettingsOnTimerEvent()
        {
            var updateCheckingTimer = new FakeUpdateCheckingTimer();
            var urlOpener = new CustomUrlOpener("{\"Enabled\": false, \"StableVersion\": \"1.0.0\", \"UpdatePageUrl\": \"http://update.com\"}");
            var updateManager = Create.UpdateManager.WithUpdateCheckingTimer(updateCheckingTimer).WithUrlOpener(urlOpener).WithCurrentVersion(new AppVersion("0.1.0")).Please();

            urlOpener.ChangeResponse("{\"Enabled\": true, \"StableVersion\": \"1.0.0\", \"UpdatePageUrl\": \"http://update.com\"}");

            updateCheckingTimer.OnNeedToCheckUpdate();

            Assert.IsTrue(updateManager.IsNeedToShowUpdateDialog);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ThrowExceptionWhenBrowserOpeningIfUpdateIsNotAvailable()
        {
            var browserOpener = new FakeBrowserOpener();
            var urlOpener = new CustomUrlOpener("{\"Enabled\": false, \"StableVersion\": \"0.1.0\", \"UpdatePageUrl\": \"http://update.com\"}");
            var updateManager = Create.UpdateManager.WithUrlOpener(urlOpener).WithBrowserOpener(browserOpener).Please();

            updateManager.OpenUpdatePage();
        }

        [Test]
        public void RecheckUpdateOnError()
        {
            var urlOpener = new CustomUrlOpener("INVALID UPDATE INFORMATION");
            var updateCheckingTimer = new FakeUpdateCheckingTimer();
            var updateManager = Create.UpdateManager.WithUrlOpener(urlOpener).WithUpdateCheckingTimer(updateCheckingTimer).WithCurrentVersion(new AppVersion("0.0.1")).Please();

            urlOpener.ChangeResponse("{\"Enabled\": true, \"StableVersion\": \"1.0.0\", \"UpdatePageUrl\": \"http://update.com\"}");

            updateCheckingTimer.OnNeedToCheckUpdateOnError();

            Assert.IsTrue(updateManager.IsNeedToShowUpdateDialog);
        }
    }
}
