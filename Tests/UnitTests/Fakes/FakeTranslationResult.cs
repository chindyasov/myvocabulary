﻿using Chindyasov.Vocabulary.TranslationProviders;

namespace UnitTests.Fakes
{
    class FakeTranslationResult : ITranslationResult
    {
        public FakeTranslationResult()
            : this("fake translation result", "Test Vendor Name, Inc.", "http://www.example.com")
        {
        }

        public FakeTranslationResult(string translationResult)
            : this(translationResult, "Test Vendor Name, Inc.", "http://www.example.com")
        {
        }

        public FakeTranslationResult(string translationResult, string vendorName, string vendorUrl)
        {
            Translation = translationResult;
            VendorName = vendorName;
            VendorUrl = vendorUrl;
        }

        public string Translation { get; set; }
        public string VendorName { get; set; }
        public string VendorUrl { get; set; }
    }
}
