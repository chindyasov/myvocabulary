﻿using Chindyasov.Vocabulary.Translator.Services;

namespace UnitTests.Fakes
{
    public class FakeBrowserOpener : IBrowserOpener
    {
        public void Open(string url)
        {
            WasOpened = true;
            LoadedUrl = url;
        }

        public bool WasOpened { get; private set; }
        public string LoadedUrl { get; private set; }
    }
}
