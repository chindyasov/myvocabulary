﻿using Chindyasov.Vocabulary.Translator;

namespace UnitTests.Fakes
{
    class FakeDimensions : IDimensions
    {
        public double ScreenHeight
        {
            get;
            set;
        }

        public double ScreenWidth
        {
            get;
            set;
        }

        public double MainViewHeight
        {
            get;
            set;
        }

        public double MainViewWidth
        {
            get;
            set;
        }
    }
}
