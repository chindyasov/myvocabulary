﻿using Chindyasov.Utils.Net;

namespace UnitTests.Fakes
{
    public class CustomUrlOpener : IUrlOpener
    {
        private string _expectedResult;

        public CustomUrlOpener(string expectedResult)
        {
            _expectedResult = expectedResult;
        }

        public void ChangeResponse(string response)
        {
            _expectedResult = response;
        }

        public string ReadToEnd(string url)
        {
            return _expectedResult;
        }
    }
}