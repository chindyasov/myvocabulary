﻿using Chindyasov.Vocabulary.Translator;
using Chindyasov.Vocabulary.Translator.ViewModels;

namespace UnitTests.Fakes
{
    public class WindowViewModelBaseFake : WindowViewModelBase
    {
        public WindowViewModelBaseFake(IDimensions dimensions) : base(dimensions)
        {
        }

        protected override void HandleEscapeKeyPressedAction()
        {
        }
    }
}
