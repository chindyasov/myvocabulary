﻿using Chindyasov.Utils.Net;

namespace UnitTests.Fakes
{
    public class UrlOpenerWithException : IUrlOpener
    {
        public string ReadToEnd(string url)
        {
            throw new UrlOperationException();
        }
    }
}