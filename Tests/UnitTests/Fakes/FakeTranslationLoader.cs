﻿using System;
using Chindyasov.Vocabulary.Translator.Models;
using Chindyasov.Vocabulary.TranslationProviders;

namespace UnitTests.Fakes
{
    class FakeTranslationLoader : ITranslationLoader
    {
        private FakeTranslationResult _translationResult;
        public event EventHandler<ITranslationResult> TranslationLoaded;
        public event EventHandler<TranslationException> TranslationError;

        public FakeTranslationLoader()
        {
            _translationResult = new FakeTranslationResult("fake translation result");
        }

        public FakeTranslationLoader(FakeTranslationResult translationResult)
        {
            _translationResult = translationResult;
        }

        public TranslationDirection LastDirection { get; private set; }

        public void StartForPhrase(string phrase, TranslationDirection direction)
        {
            LastDirection = direction;
        }

        public void RaiseTranslationReadyEvent()
        {
            TranslationLoaded?.Invoke(this, _translationResult);
        }

        public void RaiseTranslationErrorEvent()
        {
            TranslationError?.Invoke(this, new TranslationException(string.Empty));
        }
    }
}