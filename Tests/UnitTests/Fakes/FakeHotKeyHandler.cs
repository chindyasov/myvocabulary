﻿using System;
using Chindyasov.Vocabulary.Translator.Services;

namespace UnitTests.Fakes
{
    public class FakeHotKeyHandler : IHotKeyHandler
    {
        public event EventHandler HotKeyWasDetected;

        public void OnHotKeyDetected()
        {
            HotKeyWasDetected?.Invoke(this, EventArgs.Empty);
        }
    }
}
