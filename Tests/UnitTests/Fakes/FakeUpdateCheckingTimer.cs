﻿using System;
using Chindyasov.Vocabulary.Translator.Models;

namespace UnitTests.Fakes
{
    public class FakeUpdateCheckingTimer : IUpdateCheckingTimer
    {
        public event EventHandler NeedToCheckUpdate;
        public event EventHandler NeedToCheckUpdateOnError;

        public void OnNeedToCheckUpdate()
        {
            NeedToCheckUpdate?.Invoke(this, EventArgs.Empty);
        }

        internal void OnNeedToCheckUpdateOnError()
        {
            NeedToCheckUpdateOnError?.Invoke(this, EventArgs.Empty);
        }
    }
}
