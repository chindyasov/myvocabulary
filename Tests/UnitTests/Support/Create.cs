﻿using UnitTests.Support.Builders;

namespace UnitTests.Support
{
    public class Create
    {
        public static MainWindowViewModelBuilder MainWindowViewModel => new MainWindowViewModelBuilder();

        public static CloseButtonViewModelBuilder CloseButtonViewModel => new CloseButtonViewModelBuilder();

        public static PhraseInputViewModelBuilder PhraseInputViewModel => new PhraseInputViewModelBuilder();

        public static ProgressWindowViewModelBuilder ProgressWindowViewModel => new ProgressWindowViewModelBuilder();

        public static TranslationViewModelBuilder TranslationViewModel => new TranslationViewModelBuilder();

        public static MessageViewModelBuilder MessageViewModel => new MessageViewModelBuilder();

        public static ViewModelsRepositoryBuilder ViewModelsRepository => new ViewModelsRepositoryBuilder();

        public static YandexDictionaryProviderBuilder YandexDictionaryProvider => new YandexDictionaryProviderBuilder();

        public static YandexTranslateProviderBuilder YandexTranslateProvider => new YandexTranslateProviderBuilder();

        public static TranslationLoaderBuilder TranslationLoader => new TranslationLoaderBuilder();

        public static AggregateTranslationProviderBuilder AggregateTranslationProvider => new AggregateTranslationProviderBuilder();

        public static UpdateManagerBuilder UpdateManager => new UpdateManagerBuilder();

        public static UpdateWindowViewModelBuilder UpdateWindowViewModel => new UpdateWindowViewModelBuilder();
    }
}
