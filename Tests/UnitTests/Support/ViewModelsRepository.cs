﻿using Chindyasov.Vocabulary.Translator;
using Chindyasov.Vocabulary.Translator.Models;
using Chindyasov.Vocabulary.Translator.Services;
using Chindyasov.Vocabulary.Translator.ViewModels;

namespace UnitTests.Support
{
    public class ViewModelsRepository
    {
        public MainWindowViewModel MainWindowViewModel { get; set; }
        public PhraseInputViewModel PhraseInputViewModel { get; set; }
        public CloseButtonViewModel CloseButtonViewModel { get; set; }
        public ProgressWindowViewModel ProgressWindowViewModel { get; set; }
        public TranslationViewModel TranslationViewModel { get; set; }
        public MessageViewModel MessageViewModel { get; set; }
        public UpdateWindowViewModel UpdateWindowViewModel { get; set; }

        public ViewModelsRepository(IDimensions dimensions,
                                    ITranslationLoader translationLoader,
                                    IBrowserOpener browserOpener,
                                    IUpdateManager updateManager,
                                    IHotKeyHandler hotKeyHandler)
        {
            TranslationViewModel = Create.TranslationViewModel.WithDimensions(dimensions).WithBrowserOpener(browserOpener).Please();
            PhraseInputViewModel = Create.PhraseInputViewModel.Please();
            ProgressWindowViewModel = Create.ProgressWindowViewModel.WithDimensions(dimensions).Please();
            CloseButtonViewModel = Create.CloseButtonViewModel.Please();
            MessageViewModel = Create.MessageViewModel.WithDimensions(dimensions).Please();
            UpdateWindowViewModel = Create.UpdateWindowViewModel.WithDimensions(dimensions).WithUpdateManager(updateManager).Please();

            MainWindowViewModel = Create.MainWindowViewModel
                                        .WithDimensions(dimensions)
                                        .WithHotKeyHandler(hotKeyHandler)
                                        .WithCloseButtonViewModel(CloseButtonViewModel)
                                        .WithPhraseInputViewModel(PhraseInputViewModel)
                                        .WithProgressWindowViewModel(ProgressWindowViewModel)
                                        .WithTranslationLoader(translationLoader)
                                        .WithTranslationViewModel(TranslationViewModel)
                                        .WithMessageViewModel(MessageViewModel)
                                        .WithUpdateWindowViewModel(UpdateWindowViewModel)
                                        .Please();
        }
    }
}
