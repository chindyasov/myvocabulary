﻿using Chindyasov.Vocabulary.Translator;
using Chindyasov.Vocabulary.Translator.Models;
using Chindyasov.Vocabulary.Translator.ViewModels;
using UnitTests.Fakes;
using Chindyasov.Vocabulary.Translator.Services;

namespace UnitTests.Support.Builders
{
    public class MainWindowViewModelBuilder
    {
        private IDimensions _dimensions;
        private PhraseInputViewModel _phraseInputViewModel;
        private CloseButtonViewModel _closeButtonViewModel;
        private TranslationViewModel _translationViewModel;
        private ProgressWindowViewModel _progressWindowViewModel;
        private MessageViewModel _messageViewModel;
        private UpdateWindowViewModel _updateWindowViewModel;
        private ITranslationLoader _translationLoader;
        private IBrowserOpener _browserOpener;
        private IHotKeyHandler _hotKeyHandler;

        public MainWindowViewModelBuilder WithDimensions(IDimensions dimensions)
        {
            _dimensions = dimensions;
            return this;
        }

        public MainWindowViewModelBuilder WithTranslationViewModel(TranslationViewModel translationViewModel)
        {
            _translationViewModel = translationViewModel;
            return this;
        }

        public MainWindowViewModelBuilder WithProgressWindowViewModel(ProgressWindowViewModel progressWindowViewModel)
        {
            _progressWindowViewModel = progressWindowViewModel;
            return this;
        }

        public MainWindowViewModelBuilder WithCloseButtonViewModel(CloseButtonViewModel closeButtonViewModel)
        {
            _closeButtonViewModel = closeButtonViewModel;
            return this;
        }

        public MainWindowViewModelBuilder WithPhraseInputViewModel(PhraseInputViewModel phraseInputViewModel)
        {
            _phraseInputViewModel = phraseInputViewModel;
            return this;
        }

        public MainWindowViewModelBuilder WithMessageViewModel(MessageViewModel messageViewModel)
        {
            _messageViewModel = messageViewModel;
            return this;
        }

        public MainWindowViewModelBuilder WithUpdateWindowViewModel(UpdateWindowViewModel updateWindowViewModel)
        {
            _updateWindowViewModel = updateWindowViewModel;
            return this;
        }

        public MainWindowViewModelBuilder WithTranslationLoader(ITranslationLoader translationLoader)
        {
            _translationLoader = translationLoader;
            return this;
        }

        public MainWindowViewModelBuilder WithBrowserOpener(IBrowserOpener browserOpener)
        {
            _browserOpener = browserOpener;
            return this;
        }

        public MainWindowViewModelBuilder WithHotKeyHandler(IHotKeyHandler hotKeyHandler)
        {
            _hotKeyHandler = hotKeyHandler;
            return this;
        }

        public MainWindowViewModel Please()
        {
            _dimensions = _dimensions ?? new FakeDimensions() { ScreenWidth = 800, ScreenHeight = 600, MainViewWidth = 500, MainViewHeight = 50 };
            _browserOpener = _browserOpener ?? new FakeBrowserOpener();
            _translationViewModel = _translationViewModel ?? new TranslationViewModel(_dimensions, _browserOpener);
            _progressWindowViewModel = _progressWindowViewModel ?? new ProgressWindowViewModel(_dimensions);
            _phraseInputViewModel = _phraseInputViewModel ?? new PhraseInputViewModel();
            _closeButtonViewModel = _closeButtonViewModel ?? new CloseButtonViewModel();
            _messageViewModel = _messageViewModel ?? Create.MessageViewModel.WithDimensions(_dimensions).Please();
            _updateWindowViewModel = _updateWindowViewModel ?? Create.UpdateWindowViewModel.WithDimensions(_dimensions).Please();
            _translationLoader = _translationLoader ?? new FakeTranslationLoader();
            _hotKeyHandler = _hotKeyHandler ?? new FakeHotKeyHandler();

            return new MainWindowViewModel(_dimensions,
                                           _phraseInputViewModel,
                                           _closeButtonViewModel,
                                           _translationViewModel,
                                           _progressWindowViewModel,
                                           _messageViewModel,
                                           _updateWindowViewModel,
                                           _translationLoader,
                                           _hotKeyHandler);
        }
    }
}
