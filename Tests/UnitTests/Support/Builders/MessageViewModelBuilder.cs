﻿using Chindyasov.Vocabulary.Translator;
using Chindyasov.Vocabulary.Translator.ViewModels;
using UnitTests.Fakes;

namespace UnitTests.Support.Builders
{
    public class MessageViewModelBuilder
    {
        private IDimensions _dimensions;

        public MessageViewModelBuilder WithDimensions(IDimensions dimensions)
        {
            _dimensions = dimensions;
            return this;
        }

        public MessageViewModel Please()
        {
            _dimensions = _dimensions ?? new FakeDimensions();
            return new MessageViewModel(_dimensions);
        }
    }
}
