﻿using Chindyasov.Utils;
using Chindyasov.Utils.Net;
using Chindyasov.Vocabulary.Translator.Models;
using Chindyasov.Vocabulary.Translator.Services;
using UnitTests.Fakes;

namespace UnitTests.Support.Builders
{
    public class UpdateManagerBuilder
    {
        private AppVersion _currentVersion = new AppVersion("1.0.0");
        private IBrowserOpener _browserOpener;
        private IUrlOpener _urlOpener;
        private IUpdateCheckingTimer _updateCheckingTimer;

        public UpdateManagerBuilder WithBrowserOpener(IBrowserOpener browserOpener)
        {
            _browserOpener = browserOpener;
            return this;
        }

        public UpdateManagerBuilder WithCurrentVersion(AppVersion currentVersion)
        {
            _currentVersion = currentVersion;
            return this;
        }

        public UpdateManagerBuilder WithUrlOpener(IUrlOpener urlOpener)
        {
            _urlOpener = urlOpener;
            return this;
        }

        public UpdateManagerBuilder WithUpdateCheckingTimer(IUpdateCheckingTimer updateCheckingTimer)
        {
            _updateCheckingTimer = updateCheckingTimer;
            return this;
        }

        public UpdateManager Please()
        {
            _browserOpener = _browserOpener ?? new FakeBrowserOpener();
            _urlOpener = _urlOpener ?? new CustomUrlOpener("{\"Enabled\": true, \"StableVersion\": \"1.0.0\", \"UpdatePageUrl\": \"http://update.com\"}");
            _updateCheckingTimer = _updateCheckingTimer ?? new FakeUpdateCheckingTimer();

            return new UpdateManager(_updateCheckingTimer, _currentVersion, _urlOpener, _browserOpener);
        }
    }
}
