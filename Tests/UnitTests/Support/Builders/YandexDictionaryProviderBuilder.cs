﻿using Chindyasov.Utils.Net;
using Chindyasov.Vocabulary.TranslationProviders.Yandex;
using UnitTests.Fakes;

namespace UnitTests.Support.Builders
{
    public class YandexDictionaryProviderBuilder
    {
        private IUrlOpener _urlOpener;

        public YandexDictionaryProviderBuilder WithUrlOpener(IUrlOpener urlOpener)
        {
            _urlOpener = urlOpener;
            return this;
        }

        public YandexDictionaryProvider Please()
        {
            _urlOpener = _urlOpener ?? new CustomUrlOpener("{\"head\": {}, \"def\": [{\"pos\": \"noun\", \"tr\": [{\"text\": \"translation 1\"}, {\"text\": \"translation 2\"}]}]}");
            return new YandexDictionaryProvider(_urlOpener);
        }
    }
}