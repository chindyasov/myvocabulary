﻿using Chindyasov.Vocabulary.Translator;
using Chindyasov.Vocabulary.Translator.Services;
using Chindyasov.Vocabulary.Translator.ViewModels;
using UnitTests.Fakes;

namespace UnitTests.Support.Builders
{
    public class TranslationViewModelBuilder
    {
        private IDimensions _dimensions;
        private IBrowserOpener _browserOpener;

        public TranslationViewModelBuilder WithDimensions(IDimensions dimensions)
        {
            _dimensions = dimensions;
            return this;
        }

        public TranslationViewModelBuilder WithBrowserOpener(IBrowserOpener browserOpener)
        {
            _browserOpener = browserOpener;
            return this;
        }

        public TranslationViewModel Please()
        {
            _dimensions = _dimensions ?? new FakeDimensions() { ScreenWidth = 800, ScreenHeight = 600, MainViewWidth = 500, MainViewHeight = 50 };
            _browserOpener = _browserOpener ?? new FakeBrowserOpener();
            return new TranslationViewModel(_dimensions, _browserOpener);
        }
    }
}