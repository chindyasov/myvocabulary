﻿using Chindyasov.Vocabulary.TranslationProviders;

namespace UnitTests.Support.Builders
{
    public class AggregateTranslationProviderBuilder
    {
        private IDictionaryProvider _dictionaryProvider;
        private ITranslationProvider _translationProvider;

        public AggregateTranslationProviderBuilder WithTranslationProvider(ITranslationProvider translationProvider)
        {
            _translationProvider = translationProvider;
            return this;
        }

        public AggregateTranslationProviderBuilder WithDictionaryProvider(IDictionaryProvider dictionaryProvider)
        {
            _dictionaryProvider = dictionaryProvider;
            return this;
        }

        public AggregateTranslationProvider Please()
        {
            _dictionaryProvider = _dictionaryProvider ?? Create.YandexDictionaryProvider.Please();
            _translationProvider = _translationProvider ?? Create.YandexTranslateProvider.Please();

            return new AggregateTranslationProvider(_dictionaryProvider, _translationProvider);
        }
    }
}