﻿using Chindyasov.Vocabulary.Translator.ViewModels;

namespace UnitTests.Support.Builders
{
    public class PhraseInputViewModelBuilder
    {
        public PhraseInputViewModel Please()
        {
            return new PhraseInputViewModel();
        }
    }
}