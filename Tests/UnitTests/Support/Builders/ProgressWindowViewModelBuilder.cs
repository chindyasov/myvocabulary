﻿using Chindyasov.Vocabulary.Translator;
using Chindyasov.Vocabulary.Translator.ViewModels;

namespace UnitTests.Support.Builders
{
    public class ProgressWindowViewModelBuilder
    {
        private IDimensions _dimensions;

        public ProgressWindowViewModelBuilder WithDimensions(IDimensions dimensions)
        {
            _dimensions = dimensions;
            return this;
        }

        public ProgressWindowViewModel Please()
        {
            return new ProgressWindowViewModel(_dimensions);
        }
    }
}