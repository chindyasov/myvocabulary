﻿using Chindyasov.Utils.Net;
using Chindyasov.Vocabulary.TranslationProviders.Yandex;
using UnitTests.Fakes;

namespace UnitTests.Support.Builders
{
    public class YandexTranslateProviderBuilder
    {
        private IUrlOpener _urlOpener;

        public YandexTranslateProviderBuilder WithUrlOpener(IUrlOpener urlOpener)
        {
            _urlOpener = urlOpener;
            return this;
        }

        public YandexTranslateProvider Please()
        {
            _urlOpener = _urlOpener ?? new CustomUrlOpener("{\"code\":200, \"text\":[\"translation\"]}");
            return new YandexTranslateProvider(_urlOpener);
        }
    }
}