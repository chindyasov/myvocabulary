﻿using Chindyasov.Vocabulary.Translator;
using Chindyasov.Vocabulary.Translator.Models;
using Chindyasov.Vocabulary.Translator.Services;
using UnitTests.Fakes;

namespace UnitTests.Support.Builders
{
    public class ViewModelsRepositoryBuilder
    {
        private IBrowserOpener _browserOpener;
        private IDimensions _dimensions;
        private ITranslationLoader _translationLoader;
        private IUpdateManager _updateManager;
        private IHotKeyHandler _hotKeyHandler;

        public ViewModelsRepositoryBuilder WithBrowserOpener(IBrowserOpener browserOpener)
        {
            _browserOpener = browserOpener;
            return this;
        }

        public ViewModelsRepositoryBuilder WithDimensions(IDimensions dimensions)
        {
            _dimensions = dimensions;
            return this;
        }

        public ViewModelsRepositoryBuilder WithTranslationLoader(ITranslationLoader translationLoader)
        {
            _translationLoader = translationLoader;
            return this;
        }

        public ViewModelsRepositoryBuilder WithUpdateManager(IUpdateManager updateManager)
        {
            _updateManager = updateManager;
            return this;
        }

        public ViewModelsRepositoryBuilder WithHotKeyHandler(IHotKeyHandler hotKeyHandler)
        {
            _hotKeyHandler = hotKeyHandler;
            return this;
        }

        public ViewModelsRepository Please()
        {
            _browserOpener = _browserOpener ?? new FakeBrowserOpener();
            _dimensions = _dimensions ?? new FakeDimensions() { ScreenWidth = 800, ScreenHeight = 600, MainViewWidth = 500, MainViewHeight = 50 };
            _translationLoader = _translationLoader ?? new FakeTranslationLoader();
            _updateManager = _updateManager ?? Create.UpdateManager.Please();
            _hotKeyHandler = _hotKeyHandler ?? new FakeHotKeyHandler();

            return new ViewModelsRepository(_dimensions, _translationLoader, _browserOpener, _updateManager, _hotKeyHandler);
        }
    }
}