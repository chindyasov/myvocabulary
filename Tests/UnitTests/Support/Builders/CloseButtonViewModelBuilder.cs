﻿using Chindyasov.Vocabulary.Translator.ViewModels;

namespace UnitTests.Support.Builders
{
    public class CloseButtonViewModelBuilder
    {
        public CloseButtonViewModel Please()
        {
            return new CloseButtonViewModel();
        }
    }
}