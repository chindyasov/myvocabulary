﻿using Chindyasov.Vocabulary.TranslationProviders;
using Chindyasov.Vocabulary.Translator.Models;

namespace UnitTests.Support.Builders
{
    public class TranslationLoaderBuilder
    {
        private ITranslationProvider _translationProvider;

        public TranslationLoaderBuilder WithTranslationProvider(ITranslationProvider translationProvider)
        {
            _translationProvider = translationProvider;
            return this;
        }

        public TranslationLoader Please()
        {
            _translationProvider = _translationProvider ?? Create.YandexTranslateProvider.Please();

            return new TranslationLoader(_translationProvider);
        }
    }
}