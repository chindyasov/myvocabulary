﻿using Chindyasov.Vocabulary.Translator;
using Chindyasov.Vocabulary.Translator.Models;
using Chindyasov.Vocabulary.Translator.ViewModels;
using UnitTests.Fakes;

namespace UnitTests.Support.Builders
{
    public class UpdateWindowViewModelBuilder
    {
        private IDimensions _dimensions;
        private IUpdateManager _updateManager;

        public UpdateWindowViewModelBuilder WithDimensions(IDimensions dimensions)
        {
            _dimensions = dimensions;
            return this;
        }

        public UpdateWindowViewModelBuilder WithUpdateManager(IUpdateManager updateManager)
        {
            _updateManager = updateManager;
            return this;
        }

        public UpdateWindowViewModel Please()
        {
            _dimensions = _dimensions ?? new FakeDimensions() { ScreenHeight = 600, ScreenWidth = 800, MainViewHeight = 70, MainViewWidth = 500 };
            _updateManager = _updateManager ?? Create.UpdateManager.Please();

            return new UpdateWindowViewModel(_dimensions, _updateManager);
        }
    }
}