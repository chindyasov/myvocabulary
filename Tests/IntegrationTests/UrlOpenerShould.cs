﻿using Chindyasov.Utils.Net;
using NUnit.Framework;

namespace IntegrationTests
{
    [TestFixture]
    class UrlOpenerShould
    {
        [Test]
        public void ReadHtmlFromSpecifiedUrl()
        {
            var content = new UrlOpener().ReadToEnd("http://ya.ru");
            Assert.IsTrue(content.Contains("html"));
        }
    }
}
