﻿using Chindyasov.Utils.Net;
using Chindyasov.Vocabulary.TranslationProviders.Yandex;
using NUnit.Framework;

namespace IntegrationTests.TranslationProvidersTests
{
    [TestFixture]
    class YandexDictionaryProviderShould
    {
        [Test]
        public void ProvideTranslationForSingleWordFromEnToRu()
        {
            var urlOpener = new UrlOpener();
            var translationProvider = new YandexDictionaryProvider(urlOpener);
            var translation = translationProvider.Translate("mother", TranslationDirection.EN_RU).Translation;

            Assert.IsTrue(translation.Contains("мать"));
        }

        [Test]
        public void ProvideTranslationForSingleWordFromRuToEn()
        {
            var urlOpener = new UrlOpener();
            var translationProvider = new YandexDictionaryProvider(urlOpener);
            var translation = translationProvider.Translate("мама", TranslationDirection.RU_EN).Translation;

            Assert.IsTrue(translation.Contains("mother"));
        }
    }
}
