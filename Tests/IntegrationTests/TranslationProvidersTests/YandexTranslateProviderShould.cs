﻿using Chindyasov.Utils.Net;
using Chindyasov.Vocabulary.TranslationProviders.Yandex;
using NUnit.Framework;

namespace IntegrationTests.TranslationProvidersTests
{
    [TestFixture]
    class YandexTranslateProviderShould
    {
        [Test]
        public void TranslateSinglewordPhrase()
        {
            var provider = new YandexTranslateProvider(new UrlOpener());
            var translation = provider.Translate("mother", TranslationDirection.EN_RU);

            Assert.That(translation.Translation, Is.EqualTo("мать"));
        }

        [Test]
        public void TranslateMultiwordPhrase()
        {
            var provider = new YandexTranslateProvider(new UrlOpener());
            var translation = provider.Translate("hello world!", TranslationDirection.EN_RU);

            Assert.That(translation.Translation, Is.EqualTo("Всем привет!"));
        }

        [Test]
        public void TranslatePhraseInAnyForm()
        {
            var provider = new YandexTranslateProvider(new UrlOpener());
            var translation = provider.Translate("    hello     world  ! ", TranslationDirection.EN_RU);

            Assert.That(translation.Translation, Is.EqualTo("Всем привет!"));
        }

        [Test]
        public void TranslateSinglewordPhraseFromRuToEn()
        {
            var provider = new YandexTranslateProvider(new UrlOpener());
            var translation = provider.Translate("привет, мир!", TranslationDirection.RU_EN);

            Assert.That(translation.Translation, Is.EqualTo("Hello world!"));
        }
    }
}
